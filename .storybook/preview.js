import { addDecorator } from '@storybook/react';
import { MuiThemeProvider } from '@material-ui/core/styles';
import { theme } from '../src/theme/theme';

export const parameters = {
  actions: { argTypesRegex: '^on[A-Z].*' },
};

addDecorator((story) => (
  <MuiThemeProvider theme={theme}>{story()}</MuiThemeProvider>
));
