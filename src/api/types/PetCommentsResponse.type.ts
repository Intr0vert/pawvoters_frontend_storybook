import { ApiSuccessPaginatedInterface } from 'types/interfaces/common/ApiSuccess.interface';
import { CommentItemType } from 'types/pet/CommentItem.type';

export type PetCommentsResponseType = ApiSuccessPaginatedInterface<
  CommentItemType[]
>;
