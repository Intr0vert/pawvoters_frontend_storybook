import { ApiSuccessPaginatedInterface } from 'types/interfaces/common/ApiSuccess.interface';
import { SearchPost } from 'types/interfaces/SearchPost.interface';

export type SearchPostsType = ApiSuccessPaginatedInterface<SearchPost[]>;
