import { ApiSuccessPaginatedInterface } from 'types/interfaces/common/ApiSuccess.interface';
import { LeaderboardPost } from 'types/interfaces/LeaderboardPost.interface';

export type LeaderboardType = ApiSuccessPaginatedInterface<LeaderboardPost[]>;
