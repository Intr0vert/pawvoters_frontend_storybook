import { ApiSuccessPaginatedInterface } from '../../types/interfaces/common/ApiSuccess.interface';
import { ReportItemType } from '../../types/pet/ReportItem.type';

export type ReasonsResponseType = ApiSuccessPaginatedInterface<
  ReportItemType[]
>;
