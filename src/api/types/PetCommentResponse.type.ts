import { ApiSuccessInterface } from '../../types/interfaces/common/ApiSuccess.interface';
import { CommentItemType } from '../../types/pet/CommentItem.type';

export type PetCommentResponseType = ApiSuccessInterface<CommentItemType>;
