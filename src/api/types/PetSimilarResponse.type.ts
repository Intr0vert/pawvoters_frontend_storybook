import { HomePost } from 'types/interfaces/HomePost.interface';
import { ApiSuccessPaginatedInterface } from 'types/interfaces/common/ApiSuccess.interface';

export type PetSimilarResponseType = ApiSuccessPaginatedInterface<HomePost[]>;
