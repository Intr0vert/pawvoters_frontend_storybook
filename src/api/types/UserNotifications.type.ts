import { ApiSuccessPaginatedInterface } from 'types/interfaces/common/ApiSuccess.interface';
import { UserNotification } from 'types/user/UserNotification.type';

export type UserNotificationsType = ApiSuccessPaginatedInterface<
  UserNotification[]
>;
