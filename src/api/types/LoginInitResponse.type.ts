import { ApiErrorInterface } from '../../types/interfaces/common/ApiError.interface';
import { ApiSuccessInterface } from '../../types/interfaces/common/ApiSuccess.interface';

interface LoginSuccessResponse {
  token?: string;
  hash?: string;
  nextEmailInSeconds?: number;
}

export type LoginInitResponseType =
  | ApiSuccessInterface<LoginSuccessResponse>
  | ApiErrorInterface;
