import { ApiSuccessInterface } from 'types/interfaces/common/ApiSuccess.interface';
import { LastVoteCardItemInterface } from 'types/interfaces/LastVotesCardItem.interface';

export type HomeLastVotesType = ApiSuccessInterface<
  LastVoteCardItemInterface[]
>;
