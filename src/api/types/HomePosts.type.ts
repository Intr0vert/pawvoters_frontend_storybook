import { ApiSuccessPaginatedInterface } from 'types/interfaces/common/ApiSuccess.interface';
import { HomePost } from 'types/interfaces/HomePost.interface';

export type HomePostsType = ApiSuccessPaginatedInterface<HomePost[]>;

export type HomePagePetsAPI = { page: number; order?: 'popular' | 'recent' };
