import { ApiSuccessInterface } from 'types/interfaces/common/ApiSuccess.interface';
import { PetBreed } from 'types/interfaces/PetBreed.interface';

export type PetBreedsType = ApiSuccessInterface<PetBreed[]>;
