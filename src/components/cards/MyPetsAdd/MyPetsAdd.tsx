import React from 'react';
import PetsAdd from '../../icons/PetsAdd';
import styles from './MyPetsAdd.module.scss';
import { Typography } from '@material-ui/core';
import { Link } from 'react-router-dom';

const AddPet = () => {
  return (
    <Link to="/add" className={styles.cardWrapper}>
      <PetsAdd />
      <Typography variant="h4">Add pets</Typography>
    </Link>
  );
};

export default AddPet;
