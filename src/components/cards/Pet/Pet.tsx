import { Typography } from '@material-ui/core';
import { generatePetSlug } from 'lib/utils/url-utils';
import React from 'react';
import { Link } from 'react-router-dom';
import { Like } from '../../icons';
import s from './Pet.module.scss';
import { calculatePetCardHeight } from '../../../lib/utils/calculations';

export interface PetProps {
  id: number | string;
  countVotes: number | string;
  petName: string;
  userNickname: string;
  image: string | undefined;
  backgroundColor: string;
  originalImageWidth: number;
  originalImageHeight: number;
  petImageOptions?: {
    minHeightValue?: number;
    maxHeightValue?: number;
    staticHeight?: boolean;
  };
}

const Pet: React.FC<PetProps> = ({
  countVotes,
  petName,
  userNickname,
  image,
  backgroundColor,
  id,
  originalImageHeight,
  originalImageWidth,
  petImageOptions,
}) => {
  return (
    <>
      <Link
        to={`/post/${generatePetSlug(petName, id)}`}
        className={s.cardWrapper}
      >
        <div
          className={s.imageArea}
          style={{
            backgroundColor,
            paddingBottom: calculatePetCardHeight(
              originalImageWidth,
              originalImageHeight,
              petImageOptions
            ),
          }}
        >
          <img src={`${image}_600x600.jpg`} alt={petName} />
          <div className={s.votesCount}>
            <Like />
            <Typography variant="body1">{countVotes}</Typography>
          </div>
        </div>
        <div className={s.cardInfo}>
          <Typography variant="h4">{petName}</Typography>
          <Typography variant="caption">{userNickname}</Typography>
        </div>
      </Link>
    </>
  );
};

export default Pet;
