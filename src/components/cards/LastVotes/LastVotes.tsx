import React, { useEffect } from 'react';
import s from './LastVotes.module.scss';
import cn from 'classnames';
import { Typography } from '@material-ui/core';
import { useAppDispatch, useSelector } from 'store/store';
import { lastVotesSelector } from 'store/selectors';
import { fetchLastVotes } from 'store/slices/home/thunks';
import { LoadingStatusEnum } from 'types/enum/LoadingStatus.enum';
import formatDistanceStrict from 'date-fns/formatDistanceStrict';
import Avatar from 'components/ui/Avatar';

interface ILastVoteItem {
  avatar: string;
  name: string;
  votedFor: string;
  timestamp: string;
}

const LastVoteItem: React.FC<ILastVoteItem> = ({
  avatar,
  name,
  timestamp,
  votedFor,
}) => {
  return (
    <div className={s.lastVoteRoot}>
      <div className={s.userAvatar}>
        <Avatar src={avatar} alt={name} />
      </div>
      <div className={s.userInfo}>
        <Typography variant="body1">{name}</Typography>
        <Typography variant="caption">Voted for {votedFor}</Typography>
      </div>
      <div className={s.timestamp}>
        <Typography variant="overline">
          {formatDistanceStrict(new Date(timestamp), new Date(), {
            addSuffix: false,
          })}
        </Typography>
      </div>
    </div>
  );
};

const LastVotes = () => {
  const dispatch = useAppDispatch();
  const lastVotes = useSelector(lastVotesSelector);

  useEffect(() => {
    if (lastVotes.status === LoadingStatusEnum.SUCCESS) {
      return;
    }

    dispatch(fetchLastVotes());
  }, []);

  return (
    <div className={cn(s.root)}>
      {lastVotes.status === LoadingStatusEnum.SUCCESS ? (
        lastVotes.data.map((item) => (
          <LastVoteItem
            key={item.id}
            avatar={item.userImage}
            name={item.userNickname}
            timestamp={item.created}
            votedFor={item.postPetName}
          />
        ))
      ) : (
        <div className={s.placeholder} />
      )}
    </div>
  );
};

export default LastVotes;
