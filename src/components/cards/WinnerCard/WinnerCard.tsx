import { Typography } from '@material-ui/core';
import React, { useEffect, useState } from 'react';
import { ArrowLeft, ArrowRight } from '../../icons';
import styles from './WinnerCard.module.scss';
import { useAppDispatch, useSelector } from '../../../store/store';
import { homeTestimonialsSelector } from '../../../store/selectors';
import { LoadingStatusEnum } from '../../../types/enum/LoadingStatus.enum';
import { fetchTestimonialItems } from '../../../store/slices/home/thunks';

type WinnerItem = {
  id: string | number;
  pet: {
    petName: string;
    wonAmount: string | number;
    image: string | undefined;
  };
  owner: {
    avatar: string | undefined;
    ownerName: string;
    comment: string;
  };
};

export interface WinnerCardProps {
  items: WinnerItem[];
}

const WinnerCard: React.FC<WinnerCardProps> = () => {
  const [activeSlide, setActiveSlide] = useState(0);
  const testimonialsData = useSelector(homeTestimonialsSelector);
  const dispatch = useAppDispatch();

  useEffect(() => {
    if (testimonialsData.status === LoadingStatusEnum.SUCCESS) {
      return;
    }

    dispatch(fetchTestimonialItems());
  }, []);

  const handleClick = (type: 'increment' | 'decriment') => () => {
    if (type === 'decriment') {
      setActiveSlide((prevValue) =>
        prevValue === 0 ? testimonialsData.data.length - 1 : prevValue - 1
      );
    }
    if (type === 'increment') {
      setActiveSlide((prevValue) =>
        prevValue + 1 === testimonialsData.data.length ? 0 : prevValue + 1
      );
    }
  };

  if (testimonialsData.status !== LoadingStatusEnum.SUCCESS) {
    return <div className={styles.placeholder} />;
  }

  return (
    <div className={styles.winnersWrapper}>
      <div className={styles.image}>
        <img
          src={`${testimonialsData.data[activeSlide].post.image}_600x600.jpg`}
          alt={testimonialsData.data[activeSlide].post.petName}
        />
      </div>
      <div className={styles.winnerInfo}>
        {testimonialsData.data.length > 1 && (
          <div className={styles.sliderController}>
            <button
              className={styles.prevButton}
              onClick={handleClick('decriment')}
            >
              <ArrowLeft />
            </button>
            <button
              className={styles.nextButton}
              onClick={handleClick('increment')}
            >
              <ArrowRight />
            </button>
          </div>
        )}
        <div className={styles.petInfo}>
          <Typography variant="h3">
            {testimonialsData.data[activeSlide].post.petName}
          </Typography>
          <Typography variant="h4">
            {testimonialsData.data[activeSlide].prize} $
          </Typography>
        </div>
        <div className={styles.ownerInfo}>
          <div className={styles.ownerAvatar}>
            <img
              src={`${testimonialsData.data[activeSlide].user.image}_48x48.jpg`}
              alt={testimonialsData.data[activeSlide].user.nickname}
            />
          </div>
          <div className={styles.ownerColumn}>
            <Typography variant="body1">
              {testimonialsData.data[activeSlide].user.nickname}
            </Typography>
            <Typography variant="body2">
              {testimonialsData.data[activeSlide].text}
            </Typography>
          </div>
        </div>
      </div>
    </div>
  );
};

export default WinnerCard;
