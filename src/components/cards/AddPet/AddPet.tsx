import { Typography } from '@material-ui/core';
import React from 'react';
import { Add } from '../../icons';
import Button from '../../ui/Button';
import { Input } from '../../ui/Input';
import styles from './AddPet.module.scss';
import PetAddMedia from '../../common/PetAddMedia';

const AddPet = () => {
  return (
    <div className={styles.cardWrapper}>
      <PetAddMedia />
      <div className={styles.addPetForm}>
        <form>
          <Typography variant="h3">Add Your Pet</Typography>
          <Input
            label="Pet's name"
            name="add_pet"
            beginIcon={<Add color="#A4AFBE" />}
          />
          <Button>
            <span>
              <Add />
              Add Pet
            </span>
          </Button>
        </form>
      </div>
    </div>
  );
};

export default AddPet;
