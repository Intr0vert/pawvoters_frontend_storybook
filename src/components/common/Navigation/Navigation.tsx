import React from 'react';
import cn from 'classnames';
import s from './Navigation.module.scss';
import { Link } from 'react-router-dom';
import { Typography } from '@material-ui/core';

const navigationsItems = [
  {
    id: 0,
    label: 'Contest',
    to: '/contest',
  },
  {
    id: 1,
    label: 'Leaderboard',
    to: '/leaderboard',
  },
  {
    id: 2,
    label: 'My pets',
    to: '/pets',
  },
];

const Navigation = () => {
  return (
    <nav className={cn(s.navigation)}>
      {navigationsItems.map((item) => (
        <Link key={item.id} to={item.to}>
          <Typography variant="body1">{item.label}</Typography>
        </Link>
      ))}
    </nav>
  );
};

export default Navigation;
