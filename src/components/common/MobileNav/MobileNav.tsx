import React from 'react';
import s from './MobileNav.module.scss';
import { NavLink } from 'react-router-dom';
import { RoundedButton } from '../../ui/RoundedButton';
import cn from 'classnames';
import { Add, Contest, Home, Leaderboard, MyPets } from '../../icons';
import { Typography } from '@material-ui/core';

const MobileNav = () => (
  <nav className={s.root}>
    <NavLink to="/" activeClassName={s.active} className={s.navItem} exact>
      <Home />
      <Typography variant="overline">Home</Typography>
    </NavLink>
    <NavLink to="/contest" activeClassName={s.active} className={s.navItem}>
      <Contest />
      <Typography variant="overline">Contest</Typography>
    </NavLink>
    <NavLink
      to="/participate/add"
      activeClassName={s.active}
      className={s.navItem}
    >
      <RoundedButton className={cn(s.addPet)}>
        <Add />
      </RoundedButton>
    </NavLink>
    <NavLink to="/leaderboard" activeClassName={s.active} className={s.navItem}>
      <Leaderboard />
      <Typography variant="overline">Leaderboard</Typography>
    </NavLink>
    <NavLink to="/pets" activeClassName={s.active} className={s.navItem}>
      <MyPets />
      <Typography variant="overline">My pets</Typography>
    </NavLink>
  </nav>
);

export default MobileNav;
