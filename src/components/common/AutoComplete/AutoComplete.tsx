import React from 'react';
import cn from 'classnames';
import s from './AutoComplete.module.scss';
import { useAppDispatch, useSelector } from 'store/store';
import {
  searchAutoCompeleteDataSelector,
  searchSelector,
} from 'store/selectors';
import { Link, useHistory } from 'react-router-dom';
import { generatePetSlug } from 'lib/utils/url-utils';
import {
  changeActualQuery,
  changeQuery,
  resetAutoCompleteData,
} from 'store/slices/search';
import CrossedСircle from '../../icons/CrossedСircle';
import { getPosts } from 'store/slices/search/thunks';

const AutoComplete = () => {
  const searchData = useSelector(searchSelector);
  const data = useSelector(searchAutoCompeleteDataSelector);
  const dispatch = useAppDispatch();
  const history = useHistory();

  const handleReset = () => {
    dispatch(resetAutoCompleteData());
  };

  const handleClick = () => {
    dispatch(changeQuery(searchData.value.headerQuery));
    dispatch(changeActualQuery(searchData.value.headerQuery));

    dispatch(getPosts({ query: searchData.value.headerQuery }));
    history.push(`/search/posts?query=${searchData.value.headerQuery}`);

    handleReset();
  };

  return (
    <div className={cn(s.root, s.autocomplete)}>
      {data.length && searchData.autoCompleteModal ? (
        <div className={s.pets}>
          <p className={s.searchHeading}>Pets</p>
          <ul>
            {data.map((item, id) => {
              return (
                <li className={s.item} key={id}>
                  <div>
                    <Link
                      onClick={handleReset}
                      className={s.link}
                      to={{
                        pathname: `/post/${generatePetSlug(
                          item.petName,
                          item.id
                        )}`,
                      }}
                    >
                      <img className={s.avatarImg} src={item.image} />
                      <div className={s.content}>
                        <div className={s.wrapper}>
                          <div className={s.heading}>
                            <p className={s.overflowText}>{item.petName}</p>
                          </div>
                          <div className={s.subheading}>
                            <p>{item.petType}</p>
                          </div>
                        </div>
                      </div>
                    </Link>
                  </div>
                </li>
              );
            })}
          </ul>
          <div className={s.btn} onClick={handleClick}>
            See more
          </div>
        </div>
      ) : (
        <div className={s.noResult}>
          <span className={s.noResultText}>Sorry, no results found</span>
          <CrossedСircle />
        </div>
      )}
    </div>
  );
};

export default AutoComplete;
