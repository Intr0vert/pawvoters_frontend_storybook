import React, { memo, useEffect } from 'react';
import s from './LoadMore.module.scss';
import { useSelector, useAppDispatch } from 'store/store';
import { notificationSelector } from 'store/selectors/user';
import { useInView } from 'react-intersection-observer';
import { LoadingStatusEnum } from 'types/enum/LoadingStatus.enum';
import { loadMore } from 'store/slices/user/thunks';

const LoadMore = () => {
  const notification = useSelector(notificationSelector);
  const dispatch = useAppDispatch();
  const { ref, inView } = useInView({
    threshold: 0,
  });

  useEffect(() => {
    if (notification.status === LoadingStatusEnum.SUCCESS && inView) {
      dispatch(loadMore({ page: notification.pageToLoad }));
    }
  }, [inView]);

  return (
    <>
      {notification.data.length !== 0 && (
        <div className={s.root}>
          {Boolean(notification.links.next) &&
            notification.pageToLoad !== 0 && (
              <div ref={ref} className={s.observerMarker}></div>
            )}
        </div>
      )}
    </>
  );
};

export default memo(LoadMore);
