import React from 'react';
import s from './NotificationList.module.scss';
import { useSelector, useAppDispatch } from 'store/store';
import { CircularProgress } from '@material-ui/core';
import {
  notificationDataSelector,
  notificationSelector,
} from 'store/selectors/user';
import { useHistory } from 'react-router-dom';
import { setModalOpened } from 'store/slices/user';
import LoadMore from '../LoadMore';
import { LoadingStatusEnum } from 'types/enum/LoadingStatus.enum';
import NotificationContent from '../NotificationContent';

const NotificationList = () => {
  const notificationData = useSelector(notificationDataSelector);
  const notification = useSelector(notificationSelector);
  const dispatch = useAppDispatch();
  const history = useHistory();

  const handleClick = (link: string) => {
    dispatch(setModalOpened(false));
    history.push(link);
  };

  return (
    <>
      {notificationData && (
        <ul className={s.list}>
          {notificationData.map((item, id) => {
            return (
              <li
                key={id}
                className={s.item}
                onClick={() => handleClick(item.link)}
              >
                <div className={s.imgBlock}>
                  <img
                    className={s.img}
                    src={`${
                      item.data.contestImage || item.data.userImage
                    }_48x48.jpg`}
                  />
                </div>
                <NotificationContent data={notificationData} />
              </li>
            );
          })}
          <LoadMore />
          {notification.status === LoadingStatusEnum.LOADING &&
            notification.firstLoad && (
              <div className={s.preload}>
                <CircularProgress size={32} />
              </div>
            )}
        </ul>
      )}
    </>
  );
};

export default NotificationList;
