import React, { FC, useRef, useState } from 'react';
import cn from 'classnames';
import s from './Header.module.scss';
import { Container } from '@material-ui/core';
import { LogoIcon } from '../../icons';
import { Link } from 'react-router-dom';
import Navigation from '../Navigation';
import { Search } from '../Search';
import UserBlock from '../UserBlock';
import { useThrottledEvent } from 'lib/hooks/useThrottledEvent';
import AutoComplete from '../AutoComplete';
import { useSelector } from 'store/store';
import { searchSelector } from 'store/selectors';

interface Props {
  className: string;
}

const Header: FC<Props> = ({ className }) => {
  const [hasScrolled, setHasScrolled] = useState(false);
  const searchData = useSelector(searchSelector);
  const myRef = useRef<HTMLDivElement>(null);

  useThrottledEvent({
    callback: () => {
      const offset = 0;
      const { scrollTop } = document.documentElement;
      const scrolled = scrollTop > offset;
      setHasScrolled(scrolled);
    },
  });

  return (
    <header className={cn(s.header, className, { [s.hasScroll]: hasScrolled })}>
      <Container className={s.container}>
        <div className={cn(s.wrapper)}>
          <Link to="/" className={cn(s.logo)}>
            <LogoIcon />
            <span>Pawvoters</span>
          </Link>
          <Navigation />
          <div className={cn(s.search)} ref={myRef}>
            <Search myref={myRef} placeholder="Search by pet name" />
            {searchData.autoCompleteModal && <AutoComplete />}
          </div>
          <UserBlock />
        </div>
      </Container>
    </header>
  );
};

export default Header;
