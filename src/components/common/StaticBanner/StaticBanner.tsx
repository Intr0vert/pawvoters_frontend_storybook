import React from 'react';
import s from './StaticBanner.module.scss';
import cn from 'classnames';
import { Typography } from '@material-ui/core';
import StaticBannerImage from '../../icons/StaticBannerImage';
import Breadcrumbs from '../../ui/Breadcrumbs'

interface Props {
  title: string;
}

const StaticBanner: React.FC<Props> = ({ title }) => {
  return (
    <div className={cn(s.banner)}>
      <div className={cn(s.main)}>
        <Breadcrumbs items={[
          {
            id: 0,
            href: '/',
            label: 'Home',
          },
          {
            id: 1,
            href: 'https://google.com',
            label: title,
          },
        ]}>

        </Breadcrumbs>
        <Typography variant="h1">{title}</Typography>
      </div>
      <div className={cn(s.image)}>
        <StaticBannerImage />
      </div>
    </div>
  );
};

export default StaticBanner;
