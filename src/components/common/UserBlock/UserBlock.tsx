import React, { useEffect, useRef } from 'react';
import cn from 'classnames';
import s from './UserBlock.module.scss';
import { Add, LoginSignupIcon, NotificationIcon } from '../../icons';
import Button from '../../ui/Button';
import { RoundedButton } from '../../ui/RoundedButton';
import { Link, useHistory } from 'react-router-dom';
import { setModalLoginOpened } from 'store/slices/app';
import { useAppDispatch, useSelector } from 'store/store';
import Avatar from '../../ui/Avatar';
import { transformName } from '../../../lib/utils/string';
import { useUser } from 'lib/hooks/useUser';
import {
  getNotifications,
  getUnreadNotifications,
} from 'store/slices/user/thunks';
import { notificationSelector } from 'store/selectors/user';
import Notification from '../Notification/NotificationMain';
import { setModalOpened } from 'store/slices/user';
import { readNotifications } from 'lib/utils/read-notifications';

const UserBlock = () => {
  const myRef = useRef<HTMLDivElement>(null);
  const dispatch = useAppDispatch();
  const { isAuth, userData } = useUser();
  const history = useHistory();
  const notifications = useSelector(notificationSelector);

  useEffect(() => {
    dispatch(getUnreadNotifications({}));
  }, []);

  useEffect(() => {
    if (!notifications.opened) {
      dispatch(readNotifications(notifications.data));
    }
  }, [notifications.opened]);

  const handleButtonClick = (e: any) => {
    e.preventDefault();
    if (!isAuth) {
      dispatch(setModalLoginOpened({ opened: true }));
      return;
    }

    history.push('/add');
  };

  const handleModal = () => {
    if (!notifications.opened) {
      dispatch(getNotifications({ page: notifications.pageToLoad }));

      document.addEventListener('click', handleOutsideClick, false);
      document.body.style.overflow = 'hidden';
    } else {
      document.removeEventListener('click', handleOutsideClick, false);
      document.body.style.overflow = '';
    }

    dispatch(setModalOpened(!notifications.opened));
  };

  const handleOutsideClick = (e: any) => {
    if (!myRef.current?.contains(e.target)) {
      document.body.style.overflow = '';

      dispatch(setModalOpened(false));
    }
  };

  return (
    <div className={cn(s.userBlock)}>
      <Link to="/add">
        <Button className={cn(s.addPetButton)} color="secondary">
          <span>
            <Add />
            Add pet
          </span>
        </Button>
      </Link>
      <RoundedButton
        className={cn(s.addPetButtonMobile)}
        onClick={handleButtonClick}
      >
        <Add />
      </RoundedButton>
      {isAuth ? (
        <div className={s.authUser}>
          <div
            ref={myRef}
            className={cn(s.notificationBlock, {
              [s.active]: notifications.opened,
            })}
          >
            <button onClick={handleModal} className={s.notifications}>
              <NotificationIcon />
              {!notifications.unread ? null : (
                <span>{notifications.unread}</span>
              )}
            </button>
            {notifications.opened && <Notification />}
          </div>
          <div className={s.user} onClick={() => history.push('/me')}>
            <Avatar src={`${userData?.image}_48x48.jpg`} />
            <p className={s.userName}>
              Hi, {transformName(userData?.nickname)}
            </p>
          </div>
        </div>
      ) : (
        <button
          className={cn(s.loginLink)}
          type="button"
          onClick={handleButtonClick}
        >
          <span className={s.loginIcon}>
            <LoginSignupIcon />
          </span>
          <span>Login / Sign up</span>
        </button>
      )}
    </div>
  );
};

export default UserBlock;
