import React, { useState, useEffect } from 'react';
import s from './PetAddMedia.module.scss';
import PetsAdd from '../../icons/PetsAdd';
import { Typography } from '@material-ui/core';
import { createErrorToast } from 'lib/utils/toasts';
import { checkFileType } from 'lib/utils/image-types';
import PetImage from '../../ui/PetAddImage';
import { useHistory } from 'react-router';
import { petDataSelector } from 'store/selectors/index';
import { useSelector } from 'store/store';

const PetAddMedia = () => {
  const [img, setImg] = useState<string | any>('');
  const [drag, setDrag] = useState<boolean>(false);
  const history = useHistory();
  const myPetData = useSelector(petDataSelector);

  const dragStartHandler = (event: any) => {
    event.preventDefault();
    setDrag(true);
  };

  const dragLeaveHandler = (event: any) => {
    event.preventDefault();
    setDrag(false);
  };

  const dragDropHandler = (callback: any) => (event: any) => {
    event.preventDefault();
    setDrag(false);

    callback(event);
  };

  useEffect(() => {
    if (myPetData?.image && history.location.pathname !== '/add') {
      setImg(`${myPetData.image}_600x600.jpg`);
    }
  }, [myPetData]);

  const getImage = (event: any) => {
    const file = event.currentTarget.files[0];

    if (file.type)
      if (!checkFileType(file.type)) {
        setDrag(false);
        return createErrorToast({ message: 'Unsupported Format' });
      }

    // 5242880 = 5 MB
    if (file.size >= 5242880) {
      return createErrorToast({ message: 'File exceeded size, maximum 5 MB' });
    }

    if (event.currentTarget.files[0]) {
      let reader = new FileReader();

      reader.onloadend = () => {
        setImg(reader.result);
      };

      reader.readAsDataURL(event.currentTarget.files[0]);
    }

    history.push({ state: file });
  };

  dragDropHandler(getImage);

  return (
    <div className={s.petAddMedia}>
      {img ? (
        <PetImage img={img} getImage={getImage} />
      ) : (
        <div className={s.mediaWrapper}>
          <div
            className={s.dragAndDrop}
            onDrop={dragDropHandler}
            onDragOver={dragStartHandler}
            onDragLeave={dragLeaveHandler}
            onDragStart={dragStartHandler}
          >
            <input
              name="file"
              type="file"
              className={s.inputZone}
              onChange={getImage}
            />
            {drag ? (
              <>
                <PetsAdd />
                <Typography variant="h4">Drop</Typography>
                <Typography variant="overline">
                  Minimum size 640x640px, max 5MB, jpg, png
                </Typography>
              </>
            ) : (
              <>
                <PetsAdd />
                <Typography variant="h4">Pet's photo & video</Typography>
                <Typography variant="overline">
                  Minimum size 640x640px, max 5MB, jpg, png
                </Typography>
              </>
            )}
          </div>
        </div>
      )}
    </div>
  );
};

export default PetAddMedia;
