import React from 'react';
import { useUser } from 'lib/hooks/useUser';
import { Route, Redirect } from 'react-router-dom';
import { useAppDispatch } from 'store/store';
import { setModalLoginOpened } from 'store/slices/app';

const PrivateRoute = ({
  component,
  path,
  ...rest
}: {
  component: any;
  path: string;
}) => {
  const { isAuth } = useUser();
  const dispatch = useAppDispatch();

  if (!isAuth) {
    dispatch(setModalLoginOpened({ opened: true }));
  }

  return (
    <Route
      path={path}
      {...rest}
      render={() => (isAuth ? component : <Redirect to="/" />)}
    />
  );
};

export default PrivateRoute;
