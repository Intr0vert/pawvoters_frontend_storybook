import React from 'react';
import { Swiper, SwiperSlide } from 'swiper/react';
import SwiperCore, { Navigation, A11y } from 'swiper';
import 'swiper/swiper.scss';
import 'swiper/components/navigation/navigation.scss';
import { ArrowLeft, ArrowRight, SliderImage } from '../../icons';
import { Container, Typography } from '@material-ui/core';
import s from './SeoSlider.module.scss';
import cn from 'classnames';

SwiperCore.use([Navigation, A11y]);

const SeoSlider = () => {
  const [sliderState, setSliderState] = React.useState<'begin' | 'end' | null>(
    'begin'
  );
  const prevRef = React.useRef<HTMLButtonElement>(null);
  const nextRef = React.useRef<HTMLButtonElement>(null);

  const handleSliderState = (swiper: SwiperCore) => {
    if (swiper.activeIndex === 0) {
      setSliderState('begin');
      return;
    }

    if (swiper.activeIndex === 3) {
      setSliderState('end');
      return;
    }

    setSliderState(null);
  };

  return (
    <Container className={cn(s.swiperContainer)}>
      <Swiper
        grabCursor
        className={cn(s.swiperRoot, {
          [s.noLeft]: sliderState === 'begin',
          [s.noRight]: sliderState === 'end',
        })}
        spaceBetween={33}
        slidesPerView={'auto'}
        navigation={{
          prevEl: prevRef.current ? prevRef.current : undefined,
          nextEl: nextRef.current ? nextRef.current : undefined,
        }}
        onSlideChange={handleSliderState}
        onInit={(swiper) => {
          // @ts-ignore
          swiper.params.navigation.prevEl = prevRef.current;
          // @ts-ignore
          swiper.params.navigation.nextEl = nextRef.current;
          swiper.navigation.update();
        }}
      >
        <SwiperSlide className={s.imageSlide}>
          <SliderImage />
        </SwiperSlide>
        <SwiperSlide className={s.seoSlide}>
          <Typography variant="h2">Pet photo contest</Typography>
          <Typography variant="body1">
            Pupvote is a world wide pet photo contest. People from around the
            world can upload and compete by creating a profile for their loved
            pet. Pupvote is free to join and will allow you to win up to $2000
            every month in prizes.
          </Typography>
        </SwiperSlide>
        <SwiperSlide className={s.seoSlide}>
          <Typography variant="body1">
            Come to participate in the contest, vote and support your favourite
            pets or discover our blog section where we give you more information
            about your favourite breeds and you can find out which of your
            favourite breeds are featured on Pupvote.
          </Typography>
          <Typography variant="body1">
            More and more people find out that our pets pretty much want the
            same thing as we want - to love and be loved. What better way for
            your pet to receive love from every corner.
          </Typography>
        </SwiperSlide>
        <SwiperSlide className={s.seoSlide}>
          <Typography variant="body1">
            What began as a passions project to show off our own pets to the
            world, quickly transformed into one of the largest online
            communities of pet owners in the world.
          </Typography>
          <Typography variant="body1">
            We thought that if we would be so crazy about our furry friends,
            there would be more people in the world that share the same passion.
            And we were not wrong! People from all over the world started to
            pour to our site to show off their cutest pets.
          </Typography>
        </SwiperSlide>
        <SwiperSlide className={s.seoSlide}>
          <Typography variant="body1">
            We at Pupvote are proud to provide a platform for you to show of
            your closest furry friends to people around the world who care as
            much about them as you do!
          </Typography>
          <Typography variant="body1">
            We couldn't be more proud or excited to run this international pet
            photo contest and hope you will find sharing your pet just as
            excited as we find it. Hope to see you soon on Pupvote!
          </Typography>
        </SwiperSlide>
        <SwiperSlide className={ s.clearFixSlide } />
        <button
          ref={prevRef}
          className={cn(s.sliderButton, s.prevButton, {
            [s.noButton]: sliderState === 'begin',
          })}
        >
          <ArrowLeft />
        </button>
        <button
          ref={nextRef}
          className={cn(s.sliderButton, s.nextButton, {
            [s.noButton]: sliderState === 'end',
          })}
        >
          <ArrowRight />
        </button>
      </Swiper>
    </Container>
  );
};

export default SeoSlider;
