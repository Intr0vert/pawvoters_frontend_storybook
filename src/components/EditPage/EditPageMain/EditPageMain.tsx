import React from 'react';
import s from './EditPageMain.module.scss';
import EditPageForm from '../EditPageForm';
import PetAddMedia from '../../common/PetAddMedia';
import { myPetsBreed } from 'store/selectors/myPets';
import { useSelector } from 'store/store';
import { PetAddImagePlaceholder } from '../EditPlaceholder';
import { LoadingStatusEnum } from 'types/enum/LoadingStatus.enum';
import { petDataSelector } from 'store/selectors/index';

const EditPageMain = () => {
  const myPetData = useSelector(petDataSelector);
  const petBreedsData = useSelector(myPetsBreed);

  return (
    <div className={s.root}>
      {myPetData && petBreedsData.status === LoadingStatusEnum.SUCCESS ? (
        <PetAddMedia />
      ) : (
        <PetAddImagePlaceholder />
      )}
      <div className={s.editPageForm}>
        <EditPageForm />
      </div>
    </div>
  );
};

export default EditPageMain;
