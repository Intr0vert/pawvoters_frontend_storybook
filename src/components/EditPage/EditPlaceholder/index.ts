export { default as PetTypePlaceholder } from './petType';
export { default as PetNamePlaceholder } from './petName';
export { default as PetBioPlaceholder } from './petBio';
export { default as PetBreedPlaceholder } from './petBreed';
export { default as PetAddImagePlaceholder } from './petAddImage';
