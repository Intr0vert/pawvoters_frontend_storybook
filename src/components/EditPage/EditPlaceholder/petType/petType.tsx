import React from 'react';
import s from './petType.module.scss';

const petType = () => (
  <div className={s.root}>
    <span className={s.title}></span>
    <div className={s.checkbox}>
      <div>
        <span></span>
        <span className={s.checkboxTitle}></span>
      </div>
      <div>
        <span></span>
        <span className={s.checkboxTitle}></span>
      </div>
    </div>
  </div>
);

export default petType;
