import React, { useState, useEffect } from 'react';
import s from './EditPageForm.module.scss';
import FormikSelect from '../../ui/FormikSelect';
import { Typography } from '@material-ui/core';
import FormikInput from '../../ui/FormikInput';
import { Radio } from '../../ui/Radio';
import FormikRadioGroup from '../../ui/FormikRadioGroup';
import Button from '../../ui/Button';
import { useHistory } from 'react-router';
import { createErrorToast, createSuccessToast } from 'lib/utils/toasts';
import { createFormData } from 'lib/utils/form-data';
import { shallowEqual } from 'lib/utils/common';
import { Form, Formik } from 'formik';
import { useSelector } from 'store/store';
import { useAppDispatch } from 'store/store';
import * as Yup from 'yup';
import MainApiProtected from 'api/MainApiProtected';
import { myPetsBreed, myPetsBreedsData } from 'store/selectors/myPets';
import { petDataSelector } from 'store/selectors/index';
import { getBreeds } from 'store/slices/myPets/thunks';
import { resetBreed } from 'store/slices/myPets';
import {
  PetTypePlaceholder,
  PetNamePlaceholder,
  PetBioPlaceholder,
  PetBreedPlaceholder,
} from '../EditPlaceholder';
import { LoadingStatusEnum } from 'types/enum/LoadingStatus.enum';

const EditPageForm = () => {
  const dispatch = useAppDispatch();
  const petBreedsData = useSelector(myPetsBreed);
  const petBreeds = useSelector(myPetsBreedsData);
  const myPetData = useSelector(petDataSelector);
  const history = useHistory();

  const file: any = history.location.state;

  const [initialValues, setInitialValues] = useState(() => ({
    petName: myPetData?.petName || '',
    petType: myPetData?.petType || '',
    breed: myPetData?.breed.name || '',
    bio: myPetData?.bio || '',
  }));

  useEffect(() => {
    setInitialValues({
      petName: myPetData?.petName || '',
      petType: myPetData?.petType || '',
      breed: myPetData?.breed.name || '',
      bio: myPetData?.bio || '',
    });

    if (myPetData?.breed) {
      dispatch(getBreeds({ petType: myPetData?.petType }));
    }
  }, [myPetData]);

  useEffect(() => {
    return () => {
      dispatch(resetBreed());
    };
  }, [dispatch]);

  const validationSchema = Yup.object({
    petName: Yup.string()
      .min(2, 'Minimum 2 symbols')
      .max(60, 'Maximum 60 symbols')
      .required('Required field'),
    bio: Yup.string()
      .min(10, 'Minimum 10 symbols')
      .max(1000, 'Maximum 1000 symbols')
      .required('Required field'),
    breed: Yup.string().required('Select Required'),
    petType: Yup.string().required('Select Required'),
  });

  const handleSubmit = async (values: any) => {
    try {
      const result = await MainApiProtected.getInstance().editMyPet(
        createFormData({
          ...values,
          image: file,
          fbLink: 'https://www.facebook.com/paw.voters/',
          instaLink: 'https://www.instagram.com/paw.voters/',
        }),
        myPetData?.id
      );

      if (result.status !== 200) {
        throw new Error('Something went wrong, please try again');
      }

      createSuccessToast({
        message: 'Your pet has been successfully saved',
      });

      history.push('/pets');
    } catch (error) {
      if (error.data.errors.image) {
        createErrorToast({ message: `${error.data.errors.image[0]}` });
      }

      return error;
    }
  };

  return (
    <Formik
      enableReinitialize
      initialValues={initialValues}
      onSubmit={handleSubmit}
      validationSchema={validationSchema}
    >
      {({ values, initialValues, isValid, isSubmitting, setFieldValue }) => (
        <Form className={s.root}>
          {myPetData && petBreedsData.status === LoadingStatusEnum.SUCCESS ? (
            <>
              <div className={s.bottomed}>
                {/* <FormikSelect
                  label="Country"
                  name="country"
                  items={[
                    { id: 0, name: 'Country 1' },
                    { id: 1, name: 'Country 2' },
                  ]}
                /> */}
              </div>
              <Typography variant="h4" className={s.informationTitle}>
                Information about your pet
              </Typography>
              <FormikInput disabled label="Pet Name *" name="petName" />
              <div className={s.radioGroupWrapper}>
                <Typography variant="body1">Pet's type</Typography>
                <FormikRadioGroup
                  aria-label="petType"
                  name="petType"
                  value={values.petType}
                  className={s.radioGroup}
                  setFieldValue={setFieldValue}
                >
                  <Radio value="dog" label="Dog" />
                  <Radio value="cat" label="Cat" />
                </FormikRadioGroup>
              </div>
              <div className={s.bottomed}>
                <FormikSelect
                  label="Breed *"
                  value={values.breed}
                  name="breed"
                  items={petBreeds}
                />
              </div>
              <div className={s.bottomed}>
                <FormikInput label="Bio" name="bio" multiline rows="6" />
              </div>
            </>
          ) : (
            <>
              <PetNamePlaceholder />
              <PetTypePlaceholder />
              <PetBreedPlaceholder />
              <PetBioPlaceholder />
            </>
          )}
          <Button
            className={s.submitBtn}
            type="submit"
            disabled={
              (!file && shallowEqual(values, initialValues)) ||
              !isValid ||
              isSubmitting
            }
          >
            Save
          </Button>
        </Form>
      )}
    </Formik>
  );
};

export default EditPageForm;
