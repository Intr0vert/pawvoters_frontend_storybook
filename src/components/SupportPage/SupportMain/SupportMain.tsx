import React from 'react';
import s from './SupportMain.module.scss';
import SupportForm from '../SupportForm';

const SupportMain = () => {
  return (
    <div className={s.root}>
      <SupportForm />
    </div>
  );
};

export default SupportMain;
