import React, { useState, useEffect } from 'react';
import s from './SupportForm.module.scss';
import { Form, Formik } from 'formik';
import FormikSelect from '../../ui/FormikSelect';
import FormikInput from '../../ui/FormikInput';
import * as Yup from 'yup';
import Button from '../../ui/Button';
import { shallowEqual } from 'lib/utils/common';
import MainApiProtected from 'api/MainApiProtected';
import { createErrorToast, createSuccessToast } from 'lib/utils/toasts';
import { useSelector } from 'store/store';
import { getUserDataSelector } from 'store/selectors/user';
import { reasons } from './constans';

const SupportForm = () => {
  const userData = useSelector(getUserDataSelector);

  const [initialValues, setInitialValues] = useState({
    name: userData?.nickname || '',
    email: userData?.email || '',
    reason: '',
    message: '',
  });

  useEffect(() => {
    setInitialValues({
      name: userData?.nickname || '',
      email: userData?.email || '',
      reason: '',
      message: '',
    });
  }, [userData]);

  const validationSchema = Yup.object({
    name: Yup.string()
      .min(2, 'Minimum 2 symbols')
      .max(60, 'Maximum 60 symbols')
      .required('Required field'),
    email: Yup.string()
      .min(6, 'Minimum 6 symbols')
      .max(255, 'Maximum 255 symbols')
      .matches(
        /^[a-z0-9!#$%&'*+\=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+\=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)(?=.*[a-z])(?=.*[a-zA-Z])([a-zA-Z0-9]+){2}$/i,
        'Wrong email'
      )
      .required('Required field'),
    message: Yup.string()
      .min(10, 'Minimum 10 symbols')
      .max(1000, 'Maximum 500 symbols')
      .required('Required field'),
    reason: Yup.string().required('Select Required'),
  });

  const handleSubmit = async (values: any, { resetForm }: any) => {
    const result = await MainApiProtected.getInstance().postContactUs(values);

    if (result.status !== 201) {
      return createErrorToast({
        message: 'Something went wrong, please try again',
      });
    }

    createSuccessToast({ message: result.data.message });
    resetForm();
  };

  return (
    <Formik
      enableReinitialize
      initialValues={initialValues}
      onSubmit={handleSubmit}
      validationSchema={validationSchema}
    >
      {({ values, initialValues, isValid, isSubmitting }) => (
        <Form>
          <div className={s.informationTitle}>Your contact info</div>
          <div className={s.inputWrapper}>
            <FormikInput label="Name*" name="name" />
          </div>
          <div className={s.inputWrapper}>
            <FormikInput label="Email*" name="email" type="email" />
          </div>
          <div className={s.inputWrapper}>
            <FormikSelect
              label="Reason"
              value={values.reason}
              name="reason"
              items={reasons}
            />
          </div>
          <div className={s.informationTitle}>
            Please describe in detail the problem that occurred
          </div>
          <FormikInput
            label="Please define the problem*"
            name="message"
            multiline
            rows="4"
          />
          <div className={s.submitBtn}>
            <Button
              type="submit"
              disabled={
                shallowEqual(values, initialValues) || !isValid || isSubmitting
              }
            >
              Send
            </Button>
          </div>
        </Form>
      )}
    </Formik>
  );
};

export default SupportForm;
