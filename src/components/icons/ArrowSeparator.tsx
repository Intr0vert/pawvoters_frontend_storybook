import React from 'react';

const ArrowSeparator = () => {
  return (
    <svg
      width="4"
      height="6"
      viewBox="0 0 4 6"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
    >
      <path
        d="M4 2.99995L1.17133 5.82861L0.228668 4.88528L2.11467 2.99995L0.228668 1.11461L1.17133 0.17128L4 2.99995Z"
        fill="#A4AFBE"
      />
    </svg>
  );
};

export default ArrowSeparator;
