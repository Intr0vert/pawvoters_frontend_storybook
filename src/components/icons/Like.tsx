import React from 'react';

const Like = () => {
  return (
    <svg
      width="24"
      height="23"
      viewBox="0 0 24 23"
      fill="currentColor"
      xmlns="http://www.w3.org/2000/svg"
    >
      <path d="M12.0012 2.28381C14.7417 -0.17669 18.9767 -0.0950234 21.6168 2.54981C24.2558 5.19581 24.3468 9.40981 21.8922 12.1585L11.9988 22.0658L2.10782 12.1585C-0.346847 9.40981 -0.25468 5.18881 2.38315 2.54981C5.02565 -0.0915233 9.25249 -0.18019 12.0012 2.28381Z" />
    </svg>
  );
};

export default Like;
