import React, { FC } from 'react';

interface Props {
  color?: string;
}

const Add: FC<Props> = ({ color }) => {
  return (
    <svg
      width="17"
      height="17"
      viewBox="0 0 17 17"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
    >
      <path
        d="M8.49998 15.1668C4.81798 15.1668 1.83331 12.1822 1.83331 8.50016C1.83331 4.81816 4.81798 1.8335 8.49998 1.8335C12.182 1.8335 15.1666 4.81816 15.1666 8.50016C15.1666 12.1822 12.182 15.1668 8.49998 15.1668ZM7.83331 7.8335H5.16665V9.16683H7.83331V11.8335H9.16665V9.16683H11.8333V7.8335H9.16665V5.16683H7.83331V7.8335Z"
        fill={color || 'currentColor'}
      />
    </svg>
  );
};

export default Add;
