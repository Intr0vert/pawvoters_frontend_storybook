import React from 'react';

const Close = () => {
  return (
    <svg
      width="10"
      height="10"
      viewBox="0 0 10 10"
      fill="currentColor"
      xmlns="http://www.w3.org/2000/svg"
    >
      <path d="M5 4.05732L8.3 0.757324L9.24267 1.69999L5.94267 4.99999L9.24267 8.29999L8.3 9.24266L5 5.94266L1.7 9.24266L0.757334 8.29999L4.05733 4.99999L0.757334 1.69999L1.7 0.757324L5 4.05732Z" />
    </svg>
  );
};

export default Close;
