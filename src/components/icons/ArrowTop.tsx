import React from 'react';

const ArrowTop = () => (
  <svg width="28" height="28" viewBox="0 0 28 28" fill="none" xmlns="http://www.w3.org/2000/svg">
    <path
      d="M15.1667 9.13275V23.3334H12.8333V9.13275L6.57533 15.3907L4.92566 13.7411L14 4.66675L23.0743 13.7411L21.4247 15.3907L15.1667 9.13275Z"
      fill="white"/>
  </svg>
);

export default ArrowTop;