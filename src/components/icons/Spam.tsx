import React from 'react';

const Spam = () => {
  return (
    <svg
      width="16"
      height="16"
      viewBox="0 0 16 16"
      fill="currentColor"
      xmlns="http://www.w3.org/2000/svg"
    >
      <path d="M10.624 1.66669L14.3333 5.37802V10.6247L10.624 14.3334H5.37729L1.66663 10.624V5.37735L5.37729 1.66669H10.624ZM10.0713 3.00002H5.92929L3.00063 5.93002V10.072L5.92929 13.0014H10.0713L13.0006 10.072V5.92935L10.0713 3.00069V3.00002ZM7.33329 10H8.66663V11.3334H7.33329V10ZM7.33329 4.66669H8.66663V8.66669H7.33329V4.66669Z" />
    </svg>
  );
};

export default Spam;
