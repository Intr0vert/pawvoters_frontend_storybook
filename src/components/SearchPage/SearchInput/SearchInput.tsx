import React, { useEffect } from 'react';
import s from './SearchInput.module.scss';
import TextField from '@material-ui/core/TextField';
import CloseIcon from '../../icons/Close';
import { getPosts } from 'store/slices/search/thunks';
import { useAppDispatch, useSelector } from 'store/store';
import {
  changeQuery,
  clearState,
  changeActualQuery,
} from 'store/slices/search';
import Button from 'components/ui/Button';
import { searchSelector } from 'store/selectors';
import { useHistory } from 'react-router';

const SearchInputCard = () => {
  const dispatch = useAppDispatch();
  const posts = useSelector(searchSelector);
  const history = useHistory();

  const handleChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    dispatch(changeQuery(e.target.value));
  };

  const handleClearInput = () => {
    dispatch(clearState());
  };

  const handleClick = () => {
    dispatch(changeActualQuery(posts.value.query));

    if (posts.value.query) {
      dispatch(getPosts({ query: posts.value.query }));
      history.push(`/search/posts?query=${posts.value.query}`);
    }
  };

  const handleEnter = (e: any) => {
    if (e.key === 'Enter') {
      handleClick();
    }
  };

  useEffect(() => {
    const location = history.location.search;
    const query = location.slice(7);

    if (query) {
      dispatch(changeActualQuery(query));
      dispatch(changeQuery(query));
      dispatch(getPosts({ query }));
    }
  }, [dispatch, history]);

  return (
    <div className={s.inputCard}>
      <div className={s.searchWrapper}>
        <TextField
          className={s.searchInput}
          id="outlined-basic"
          fullWidth={true}
          label="Search"
          variant="outlined"
          value={posts.value.query}
          onChange={handleChange}
          onKeyPress={handleEnter}
        />
        <i className={s.closeIcon}>
          {!posts.value.query ? (
            <span></span>
          ) : (
            <button className={s.closeButton} onClick={handleClearInput}>
              <CloseIcon />
            </button>
          )}
        </i>
      </div>
      <Button className={s.searchButton} onClick={handleClick}>
        Search
      </Button>
    </div>
  );
};

export default SearchInputCard;
