import React from 'react';
import { searchDataSelector } from 'store/selectors';
import { useSelector } from 'store/store';
import cn from 'classnames';
import s from './SearchList.module.scss';
import { Link } from 'react-router-dom';
import { generatePetSlug } from 'lib/utils/url-utils';

const SearchList = () => {
  const data = useSelector(searchDataSelector);

  return (
    <div className={s.root}>
      <ul className={cn(s.list, s.search)}>
        {data.map((post, id) => {
          return (
            <li className={s.item} key={id}>
              <div>
                <Link
                  className={s.link}
                  to={{
                    pathname: `/post/${generatePetSlug(post.petName, post.id)}`,
                  }}
                >
                  <img className={s.avatarImg} src={post.image} />
                  <div className={s.content}>
                    <div className={s.wrapper}>
                      <div className={s.heading}>
                        <p className={s.overflowText}>{post.petName}</p>
                      </div>
                      <div className={s.subheading}>
                        <p>{post.petType}</p>
                      </div>
                    </div>
                  </div>
                </Link>
              </div>
            </li>
          );
        })}
      </ul>
    </div>
  );
};

export default SearchList;
