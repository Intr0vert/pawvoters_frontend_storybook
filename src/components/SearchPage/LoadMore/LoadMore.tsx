import React from 'react';
import s from './LoadMore.module.scss';
import { loadMore } from 'store/slices/search/thunks';
import { useSelector, useAppDispatch } from 'store/store';
import { searchSelector } from 'store/selectors';
import { LoadMore as LoadMoreIcon } from 'components/icons';
import Button from 'components/ui/Button';
import { LoadingStatusEnum } from 'types/enum/LoadingStatus.enum';

const LoadMore = () => {
  const posts = useSelector(searchSelector);
  const dispatch = useAppDispatch();

  const handleClick = () => {
    dispatch(
      loadMore({
        query: posts.value.actualQuery,
        page: posts.pageToLoad,
      })
    );
  };

  return (
    <>
      {posts.data.length ? (
        <div className={s.root}>
          {Boolean(posts.links.next) && (
            <div className={s.container}>
              {posts.status === LoadingStatusEnum.SUCCESS && (
                <Button onClick={handleClick}>
                  <LoadMoreIcon />
                  Show more
                </Button>
              )}
            </div>
          )}
        </div>
      ) : null}
    </>
  );
};

export default LoadMore;
