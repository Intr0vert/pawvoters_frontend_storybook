import React from 'react';
import s from './EmptySearch.module.scss';
import { searchSelector } from 'store/selectors';
import { useSelector } from 'store/store';
import { LoadingStatusEnum } from 'types/enum/LoadingStatus.enum';
import Magnifier from '../../icons/Magnifier';
import CrossedСircle from '../../icons/CrossedСircle';

const EmptySearch = () => {
  const posts = useSelector(searchSelector);

  return (
    <>
      {posts.status === LoadingStatusEnum.IDLE && (
        <div className={s.root}>
          <i className={s.icon}>
            <Magnifier />
          </i>
          <span>Enter search query</span>
        </div>
      )}
      {posts.status === LoadingStatusEnum.SUCCESS && !posts.data.length && (
        <div className={s.root}>
          <i className={s.icon}>
            <CrossedСircle />
          </i>
          <span>Sorry, no results found</span>
        </div>
      )}
    </>
  );
};

export default EmptySearch;
