import React from 'react';
import s from './SearchListMain.module.scss';
import { searchSelector } from 'store/selectors';
import { useSelector } from 'store/store';
import { LoadingStatusEnum } from 'types/enum/LoadingStatus.enum';
import { CircularProgress } from '@material-ui/core';
import LoadMore from 'components/SearchPage/LoadMore';
import EmptySearch from '../EmptySearch';
import SearchList from '../SearchList';

const SearchCard = () => {
  const posts = useSelector(searchSelector);

  return (
    <div className={s.root}>
      {posts.status == LoadingStatusEnum.LOADING ? (
        <p className={s.searchResult}>Search results</p>
      ) : posts.data.length ? (
        <p className={s.searchResult}>Search results</p>
      ) : null}
      <EmptySearch />
      {posts.data.length ? <SearchList /> : null}
      <LoadMore />
      {posts.status === LoadingStatusEnum.LOADING && (
        <div className={s.preload}>
          <CircularProgress size={55} />
        </div>
      )}
    </div>
  );
};

export default SearchCard;
