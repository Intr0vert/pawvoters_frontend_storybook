import { Typography } from '@material-ui/core';
import { Like, PlaceIcon } from 'components/icons';
import StatItem from 'components/PetPage/common/StatItem';
import Image from 'components/ui/Image';
import React from 'react';
import image from 'static/images/development/pet.jpg';
import s from './PetInfo.module.scss';

const PetInfo = () => {
  return (
    <div className={s.petInfo}>
      <div className={s.petImage}>
        <Image src={image} />
      </div>
      <div className={s.petInfoCol}>
        <Typography variant="overline" className={s.petBreed}>
          German Shepherd Cat
        </Typography>
        <Typography variant="h4" className={s.petName}>
          Max
        </Typography>
        <div className={s.petStats}>
          <StatItem icon={<Like />} number={0} type="likes" size="small" />
          <StatItem
            icon={<PlaceIcon />}
            number={1456}
            sufix
            type="position"
            size="small"
          />
        </div>
      </div>
    </div>
  );
};

export default PetInfo;
