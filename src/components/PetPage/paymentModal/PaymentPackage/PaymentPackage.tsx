import { Typography } from '@material-ui/core';
import { Like } from 'components/icons';
import React from 'react';
import s from './PaymentPackage.module.scss';
import cn from 'classnames';
import Button from 'components/ui/Button';
import { PaymentPackagesEnum } from 'types/enum/PaymentPackages.enum';

interface PaymentPackageProps {
  type: PaymentPackagesEnum;
  price: string;
  votesCount: number;
  handleClick: () => void;
}

const PaymentPackage: React.FC<PaymentPackageProps> = ({
  price,
  type,
  votesCount,
  handleClick,
}) => {
  return (
    <div className={cn(s.root, s[type])}>
      <div className={s.like}>
        <Like />
      </div>
      <div className={s.price}>
        <Typography variant="body2">{price}</Typography>
      </div>
      <div className={s.button}>
        <Button size="small" onClick={handleClick}>
          +{votesCount} vote
        </Button>
      </div>
    </div>
  );
};

export default PaymentPackage;
