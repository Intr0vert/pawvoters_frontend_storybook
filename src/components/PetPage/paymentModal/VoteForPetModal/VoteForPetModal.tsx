import { Typography } from '@material-ui/core';
import React, { useState } from 'react';
import s from './VoteForPetModal.module.scss';
import { PaymentPackagesEnum } from 'types/enum/PaymentPackages.enum';
import PaymentPackage from '../PaymentPackage';
import PetInfo from '../PetInfo';
import PaymentStep from '../PaymentStep';

const packages = [
  {
    id: 0,
    type: PaymentPackagesEnum.FREE,
    votesCount: 1,
    price: 'Free',
  },
  {
    id: 1,
    type: PaymentPackagesEnum.REGULAR,
    votesCount: 100,
    price: '$1.99',
  },
  {
    id: 2,
    type: PaymentPackagesEnum.REGULAR,
    votesCount: 300,
    price: '$4.99',
  },
  {
    id: 3,
    type: PaymentPackagesEnum.SPECIAL,
    votesCount: 500,
    price: '$4.49',
  },
  {
    id: 4,
    type: PaymentPackagesEnum.REGULAR,
    votesCount: 1000,
    price: '$9.99',
  },
  {
    id: 5,
    type: PaymentPackagesEnum.REGULAR,
    votesCount: 3000,
    price: '$18.99',
  },
];

const VoteForPetModal = () => {
  const [paymentStep, setPaymentStep] = useState<
    'initial' | 'payment' | 'success' | 'fail'
  >('initial');

  const handleClick = () => {
    setPaymentStep('payment');
  };

  return (
    <div className={s.root}>
      <Typography variant="h3" className={s.title}>
        Vote for Pet
      </Typography>
      <PetInfo />
      {paymentStep === 'initial' && (
        <div className={s.packagesRoot}>
          {packages.map((item) => (
            <PaymentPackage key={item.id} handleClick={handleClick} {...item} />
          ))}
        </div>
      )}
      {paymentStep === 'payment' && <PaymentStep />}
    </div>
  );
};

export default VoteForPetModal;
