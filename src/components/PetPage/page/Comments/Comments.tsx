import { CircularProgress, Typography } from '@material-ui/core';
import {
  LoadMore as LoadMoreIcon,
  RemoveIcon,
  ReplyIcon,
  SpamIcon,
} from 'components/icons';
import Avatar from 'components/ui/Avatar';
import React, { useState } from 'react';
import s from './Comments.module.scss';
import cn from 'classnames';
import Button from 'components/ui/Button';
import CommentForm from '../CommentForm';
import { useAppDispatch, useSelector } from 'store/store';
import {
  petCommentsSelector,
  petDataSelector,
  petReportReasonsSelector,
} from 'store/selectors';
import { LoadingStatusEnum } from 'types/enum/LoadingStatus.enum';
import { CommentItemType } from 'types/pet/CommentItem.type';
import { ReplyItemType } from 'types/pet/ReplyItem.type';
import formatDistance from 'date-fns/formatDistance';
import { deletePostComment, fetchPostComments } from 'store/slices/pet/thunks';
import { getUserDataSelector } from 'store/selectors/user';
import { createErrorToast, createSuccessToast } from 'lib/utils/toasts';
import Menu from '@material-ui/core/Menu';
import MenuItem from '@material-ui/core/MenuItem';
import {
  bindMenu,
  bindTrigger,
  usePopupState,
} from 'material-ui-popup-state/hooks';
import { useStyles } from './styles';
import { useUser } from 'lib/hooks/useUser';
import MainApiProtected from 'api/MainApiProtected';
import { updateIsReportedCommentState } from 'store/slices/pet';

interface CommentController {
  set: React.Dispatch<React.SetStateAction<number | null>>;
  current: number | null;
}

const CommentItem: React.FC<
  ReplyItemType & { isReply?: boolean; parentId?: number } & {
    commentController?: CommentController;
  }
> = ({
  user,
  created,
  text,
  isAlreadyReportedByMe,
  isReply = false,
  id,
  commentController,
  parentId,
}) => {
  const { isAuth } = useUser();
  const [isReporting, setIsReporting] = useState(false);
  const dispatch = useAppDispatch();
  const petData = useSelector(petDataSelector);
  const userData = useSelector(getUserDataSelector);
  const petCommentsData = useSelector(petCommentsSelector);
  const reportReasonsData = useSelector(petReportReasonsSelector);
  const popupState = usePopupState({
    variant: 'popover',
    popupId: 'reportMenu',
  });
  const classes = useStyles();
  const isOwner = user.id === userData?.id;

  const handleClick = () => {
    commentController?.set(id);
  };

  const handleDelete = () => {
    if (petData && petCommentsData.isDeleting !== LoadingStatusEnum.LOADING) {
      dispatch(
        deletePostComment({ postId: petData.id, commentId: id, parentId })
      ).then((result) => {
        if (result.payload?.status === 200) {
          createSuccessToast({ message: 'You have deleted your comment' });
        }

        if (result.payload?.status !== 200) {
          createSuccessToast({
            message: 'Something went wrong, please try again later',
          });
        }
      });
    }
  };

  const handleReportClick = (reason: string) => async () => {
    popupState.close();
    setIsReporting(true);
    try {
      await MainApiProtected.getInstance().reportCommentById(id, reason);
      dispatch(
        updateIsReportedCommentState({ commentId: id, parentId, result: true })
      );
      createSuccessToast({ message: 'Report send to administrator' });
    } catch (error) {
      createErrorToast({
        message: 'Something went wrong, please try again later',
      });
      dispatch(
        updateIsReportedCommentState({ commentId: id, parentId, result: false })
      );
    } finally {
      setIsReporting(false);
    }
  };

  return (
    <div className={s.commentItemRoot}>
      <div className={s.commentItemUser}>
        <Avatar src={`${user.image}`} />
        <div className={s.commentItemInfo}>
          <Typography variant="caption" className={s.commentItemTime}>
            {formatDistance(new Date(created), new Date())}
          </Typography>
          <Typography variant="body1" className={s.commentItemAuthor}>
            {user.nickname}
          </Typography>
        </div>
      </div>
      <div className={s.commentItemContent}>
        <Typography variant="body1">{text}</Typography>
      </div>
      <div className={s.commentItemControls}>
        {!isReply && (
          <button
            className={cn(s.commentItemControlButton, s.commentItemReplyButton)}
            onClick={handleClick}
          >
            <ReplyIcon />
            <span>Reply</span>
          </button>
        )}
        {isOwner && (
          <button className={s.commentItemControlButton} onClick={handleDelete}>
            <RemoveIcon />
            <span>Delete</span>
          </button>
        )}
        {!isAlreadyReportedByMe && !isOwner && isAuth && (
          <>
            <button
              className={cn(s.commentItemControlButton, {
                [s.commentItemControlButtonActive]: popupState.isOpen,
              })}
              disabled={
                reportReasonsData.status !== LoadingStatusEnum.SUCCESS ||
                isReporting
              }
              {...bindTrigger(popupState)}
            >
              <SpamIcon />
              <span>Report</span>
            </button>
            <Menu
              {...bindMenu(popupState)}
              anchorOrigin={{
                vertical: 'bottom',
                horizontal: 'center',
              }}
              transformOrigin={{
                vertical: 'top',
                horizontal: 'center',
              }}
              classes={{
                paper: classes.paper,
                list: classes.list,
              }}
            >
              {reportReasonsData.data.map((item) => (
                <MenuItem
                  onClick={handleReportClick(item.code)}
                  key={item.code}
                >
                  {item.title}
                </MenuItem>
              ))}
            </Menu>
          </>
        )}
      </div>
    </div>
  );
};

const CommentWrapper: React.FC<
  {
    item: CommentItemType;
  } & {
    commentController?: CommentController;
  }
> = ({ item, commentController }) => {
  const [loadingReplies, setLoadingReplies] = useState(false);
  const dispatch = useAppDispatch();
  const petData = useSelector(petDataSelector);

  const handleLoadingReplies = () => {
    if (petData) {
      setLoadingReplies(true);
      dispatch(
        fetchPostComments({
          postId: petData.id,
          parentId: item.id,
          page: Math.floor(item.replies.length / 6) + 1,
        })
      ).then((result) => {
        setLoadingReplies(false);
      });
    }
  };

  return (
    <div className={s.commentWrapper}>
      <CommentItem {...item} commentController={commentController} />
      {commentController?.current === item.id && (
        <div className={cn(s.commentReplies, s.commentFormWrapper)}>
          <CommentForm parentId={item.id} />
        </div>
      )}
      {Boolean(item.replies.length) && (
        <div
          className={cn(s.commentReplies, {
            [s.commentsNoMoreReplies]: item.replies.length <= 2,
          })}
        >
          {item.replies.map((reply) => (
            <CommentItem key={reply.id} {...reply} isReply />
          ))}
          {loadingReplies ? (
            <div className={s.loadingMoreReplies}>
              <CircularProgress size={55} />
            </div>
          ) : (
            item.replies.length < item.totalReplies && (
              <Button
                className={s.loadMoreReplies}
                onClick={handleLoadingReplies}
              >
                <LoadMoreIcon />
                Load more
              </Button>
            )
          )}
        </div>
      )}
    </div>
  );
};

const LoadMoreCommentsFactory = () => {
  const petCommentsData = useSelector(petCommentsSelector);
  const petData = useSelector(petDataSelector);
  const dispatch = useAppDispatch();

  const handleLoadMoreClick = () => {
    if (petData) {
      dispatch(fetchPostComments({ postId: petData.id }));
    }
  };

  if (!petCommentsData.isMore) {
    return null;
  }

  if (petCommentsData.loadingMoreStatus === LoadingStatusEnum.LOADING) {
    return (
      <div className={s.loadingMoreComments}>
        <CircularProgress size={55} />
      </div>
    );
  }

  return (
    <Button className={s.loadMoreComments} onClick={handleLoadMoreClick}>
      <LoadMoreIcon />
      Load more
    </Button>
  );
};

const Comments = () => {
  const petCommentsData = useSelector(petCommentsSelector);

  const [activeCommentId, setActiveCommentId] = useState<number | null>(null);

  return (
    <div className={s.root}>
      {petCommentsData.data.map((item) => {
        return (
          <CommentWrapper
            key={item.id}
            item={item}
            commentController={{
              set: setActiveCommentId,
              current: activeCommentId,
            }}
          />
        );
      })}
      <LoadMoreCommentsFactory />
    </div>
  );
};

export default Comments;
