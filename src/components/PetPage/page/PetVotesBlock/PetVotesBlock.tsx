import { Like, PlaceIcon } from 'components/icons';
import React from 'react';
import s from './PetVotesBlock.module.scss';
import Button from 'components/ui/Button';
import PetLastVotes from '../PetLastVotes';
import { useAppDispatch, useSelector } from 'store/store';
import { handlePetModalState } from 'store/slices/pet';
import StatItem from '../../common/StatItem';
import { petDataSelector } from 'store/selectors';

const PetVotesBlock = () => {
  const dispatch = useAppDispatch();
  const petData = useSelector(petDataSelector);

  const handleClick = () => {
    dispatch(handlePetModalState({ opened: true }));
  };

  return (
    <div className={s.root}>
      {petData ? (
        <div className={s.stats}>
          <StatItem
            icon={<Like />}
            number={petData?.countVotes || 0}
            type="likes"
          />
          <StatItem
            icon={<PlaceIcon />}
            number={petData?.countVotes || 0}
            sufix
            type="position"
          />
        </div>
      ) : (
        <div className={s.statsPlaceholder}>
          <div className={s.statItemPlaceholder}>
            <span></span>
          </div>
          <div className={s.statItemPlaceholder}>
            <span></span>
          </div>
        </div>
      )}
      {petData ? (
        <Button onClick={handleClick}>Vote for Pet</Button>
      ) : (
        <div className={s.buttonPlaceholder} />
      )}
      <PetLastVotes />
    </div>
  );
};

export default PetVotesBlock;
