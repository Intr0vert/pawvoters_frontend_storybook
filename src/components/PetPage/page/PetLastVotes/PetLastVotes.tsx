import { Typography } from '@material-ui/core';
import Avatar from 'components/ui/Avatar';
import React from 'react';
import s from './PetLastVotes.module.scss';
import testImage from 'static/images/development/pet_post.jpg';
import { useSelector } from 'store/store';
import { petVotesSelector } from 'store/selectors';
import { LoadingStatusEnum } from '../../../../types/enum/LoadingStatus.enum';

const PetVotesFactory = () => {
  const petVotesData = useSelector(petVotesSelector);

  if (petVotesData.status !== LoadingStatusEnum.SUCCESS) {
    return (
      <div className={s.avatarListPlaceholder}>
        <span />
        <span />
        <span />
      </div>
    );
  }

  if (petVotesData.data.length === 0) {
    return (
      <div className={s.noVotes}>
        <Avatar src={testImage} />
        <Typography variant="body1" className={s.noVotesCaption}>
          Be the first!
        </Typography>
      </div>
    );
  }

  return (
    <div className={s.avatarList}>
      {petVotesData.data.map((user) => (
        <Avatar key={user.id} src={`${user.userImage}_48x48.jpg`} />
      ))}
    </div>
  );
};

const PetLastVotes = () => {
  return (
    <div className={s.root}>
      <Typography variant="caption" className={s.title}>
        Last votes
      </Typography>
      <PetVotesFactory />
    </div>
  );
};

export default PetLastVotes;
