import { CircularProgress, Typography } from '@material-ui/core';
import { NoCommentsImage } from 'components/icons';
import React from 'react';
import Comments from '../Comments';
import CommentForm from '../CommentForm';
import s from './PetComments.module.scss';
import { useSelector } from 'store/store';
import { petCommentsSelector } from 'store/selectors';
import { LoadingStatusEnum } from '../../../../types/enum/LoadingStatus.enum';

const NoComments = () => {
  return (
    <div className={s.noCommentsRoot}>
      <NoCommentsImage />
      <Typography variant="h4" className={s.noCommentsTitle}>
        Be the first to comment!
      </Typography>
    </div>
  );
};

const PetCommentsFactory = () => {
  const petCommentsData = useSelector(petCommentsSelector);

  if (petCommentsData.status === LoadingStatusEnum.LOADING) {
    return (
      <div className={s.preloader}>
        <CircularProgress size={55} />
      </div>
    );
  }

  if (petCommentsData.data.length === 0) {
    return <NoComments />;
  }

  return (
    <>
      <div className={s.isComments} />
      <Comments />
    </>
  );
};

const PetComments = () => {
  return (
    <div className={s.root}>
      <Typography variant="h4">Comments</Typography>
      <CommentForm global />
      <PetCommentsFactory />
    </div>
  );
};

export default PetComments;
