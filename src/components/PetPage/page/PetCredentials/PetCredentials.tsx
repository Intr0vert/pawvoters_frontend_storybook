import React from 'react';
import s from './PetCredentials.module.scss';
import { Typography } from '@material-ui/core';
import Instagram from 'components/icons/Instagram';
import Facebook from 'components/icons/Facebook';
import { useSelector } from 'store/store';
import { petDataSelector } from 'store/selectors';

const PetCredentials = () => {
  const petData = useSelector(petDataSelector);

  return (
    <div className={s.root}>
      {petData ? (
        <>
          <Typography variant="body2" className={s.breed}>
            {petData?.breed.name}
          </Typography>
          <Typography variant="h1">{petData?.petName}</Typography>
        </>
      ) : (
        <>
          <div className={s.breedPlaceholder}>
            <span></span>
          </div>
          <div className={s.petNamePlaceholder}>
            <span></span>
          </div>
        </>
      )}
      {petData ? (
        <>
          {(petData.fbLink || petData.instaLink) && (
            <div className={s.socials}>
              {petData.fbLink && (
                <a href={petData.fbLink} target="_blank">
                  <i>
                    <Facebook />
                  </i>
                </a>
              )}
              {petData.instaLink && (
                <a href={petData.instaLink} target="_blank">
                  <i>
                    <Instagram />
                  </i>
                </a>
              )}
            </div>
          )}
        </>
      ) : (
        <div className={s.socialsPlaceholder}>
          <span></span>
          <span></span>
        </div>
      )}
    </div>
  );
};

export default PetCredentials;
