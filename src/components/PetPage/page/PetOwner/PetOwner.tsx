import { Typography } from '@material-ui/core';
import Avatar from 'components/ui/Avatar';
import React from 'react';
import s from './PetOwner.module.scss';
import { petDataSelector } from 'store/selectors';
import { useSelector } from 'store/store';

const PetOwner = () => {
  const petData = useSelector(petDataSelector);

  return (
    <div className={s.root}>
      {petData ? (
        <div className={s.userAvatar}>
          <Avatar src={`${petData.user.image}_48x48.jpg`} />
        </div>
      ) : (
        <div className={s.avatarPlaceholder} />
      )}
      {petData ? (
        <div className={s.userInfo}>
          <Typography variant="body2" className={s.name}>
            {petData.user.nickname}
          </Typography>
          <Typography variant="caption" className={s.location}>
            {petData.user.city}
          </Typography>
        </div>
      ) : (
        <div className={s.userInfoPlaceholder}>
          <div className={s.namePlaceholder}>
            <span></span>
          </div>
          <div className={s.locationPlaceholder}>
            <span></span>
          </div>
        </div>
      )}
    </div>
  );
};

export default PetOwner;
