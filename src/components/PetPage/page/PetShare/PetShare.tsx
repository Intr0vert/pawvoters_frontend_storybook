import { Close, FacebookIcon, Twitter, ShareIcon } from 'components/icons';
import { RoundedButton } from 'components/ui/RoundedButton';
import React, { useState } from 'react';
import { useTransition, animated } from 'react-spring';
import s from './PetShare.module.scss';
import { FacebookShareButton, TwitterShareButton } from 'react-share';

const PetShare = () => {
  const [show, set] = useState(false);
  const transitions = useTransition(show, null, {
    from: {
      position: 'absolute',
      width: 0,
      opacity: 0,
      transform: 'translate3d(-15px,0,0)',
    },
    enter: { width: 229, opacity: 1, transform: 'translate3d(0,0,0)' },
    leave: { width: 0, opacity: 0, transform: 'translate3d(-15px,0,0)' },
  });

  const handleClick = () => {
    set(!show);
  };

  return (
    <div className={s.root}>
      <RoundedButton onClick={handleClick}>
        <ShareIcon />
      </RoundedButton>
      {transitions.map(
        ({ item, key, props }) =>
          item && (
            <animated.div className={s.shareList} key={key} style={props}>
              <FacebookShareButton url="https://facebook.com/123">
                <FacebookIcon />
              </FacebookShareButton>
              <TwitterShareButton url="https://twitter.com/123">
                <Twitter />
              </TwitterShareButton>
              <button
                type="button"
                className={s.closeShare}
                onClick={handleClick}
              >
                <Close />
              </button>
            </animated.div>
          )
      )}
    </div>
  );
};

export default PetShare;
