import React from 'react';
import s from './PetImage.module.scss';
import Image from 'components/ui/Image';
import { useSelector } from 'store/store';
import { petDataSelector } from 'store/selectors';

const PetImage = () => {
  const petData = useSelector(petDataSelector);

  return (
    <>
      {petData ? (
        <div className={s.petImage}>
          <Image src={`${petData.image}_600x600.jpg`} />
        </div>
      ) : (
        <div className={s.petImage}>
          <div className={s.placeholder}></div>
        </div>
      )}
    </>
  );
};

export default PetImage;
