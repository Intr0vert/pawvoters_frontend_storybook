import React from 'react';
import s from './PetInfoBlock.module.scss';
import cn from 'classnames';
import PetImage from '../PetImage';
import PetCredentials from '../PetCredentials';
import PetOwner from '../PetOwner';
import PetVotesBlock from '../PetVotesBlock';
import PetShare from '../PetShare';
import PetBio from '../PetBio';

const PetInfoBlock = () => {
  return (
    <>
      <div className={cn(s.root)}>
        <PetImage />
        <div className={cn(s.petInfoCol)}>
          <PetCredentials />
          <PetOwner />
          <PetVotesBlock />
          <PetShare />
        </div>
      </div>
      <PetBio />
    </>
  );
};

export default PetInfoBlock;
