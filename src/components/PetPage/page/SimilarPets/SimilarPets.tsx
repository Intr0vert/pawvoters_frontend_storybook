import Pet from 'components/cards/Pet';
import React from 'react';
import StackGrid from 'react-stack-grid';
import s from './SimilarPets.module.scss';
import image1 from 'static/images/development/pet.jpg';
import { useSelector } from '../../../../store/store';
import { petSimilarSelector } from '../../../../store/selectors';

const SimilarPets = () => {
  const similarPetsData = useSelector(petSimilarSelector);

  return (
    <div className={s.root}>
      <StackGrid
        //@ts-ignore
        // gridRef={(grid) => (gridRef.current = grid)}
        monitorImagesLoaded
        columnWidth="33.33%"
        gutterWidth={30}
        gutterHeight={30}
        appearDelay={0}
        duration={0}
      >
        {similarPetsData.data.map((item) => (
          <Pet
            key={item.id}
            {...item}
            petImageOptions={{
              staticHeight: true,
            }}
          />
        ))}
      </StackGrid>
    </div>
  );
};

export default SimilarPets;
