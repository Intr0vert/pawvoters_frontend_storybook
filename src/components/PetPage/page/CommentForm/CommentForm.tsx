import { SendIcon } from 'components/icons';
import Avatar from 'components/ui/Avatar';
import { Input } from 'components/ui/Input';
import { RoundedButton } from 'components/ui/RoundedButton';
import React, { useState } from 'react';
import s from './CommentForm.module.scss';
import { useAppDispatch, useSelector } from 'store/store';
import { petCommentsSelector, petDataSelector } from 'store/selectors';
import { LoadingStatusEnum } from 'types/enum/LoadingStatus.enum';
import { setModalLoginOpened } from 'store/slices/app';
import User from 'lib/User';
import { createPostComment } from 'store/slices/pet/thunks';
import { createErrorToast } from 'lib/utils/toasts';

interface CommentFormProps {
  global?: boolean;
  parentId?: number;
}

const CommentForm: React.FC<CommentFormProps> = ({ global, parentId }) => {
  const petData = useSelector(petDataSelector);
  const petCommentsData = useSelector(petCommentsSelector);
  const dispatch = useAppDispatch();
  const [value, setValue] = useState('');

  const handleComment = () => {
    if (!User.isUser()) {
      dispatch(setModalLoginOpened({ opened: true }));
      return;
    }

    if (!global && petData && parentId) {
      console.log('reply add');

      dispatch(
        createPostComment({ postId: petData.id, text: value, parentId })
      ).then((result) => {
        if (result.payload?.status !== 201) {
          createErrorToast({
            message: 'Something went wrong, please try again later',
          });
          return;
        }

        if (result?.payload?.status === 201) {
          setValue('');
        }
      });

      return;
    }

    if (petData) {
      dispatch(createPostComment({ postId: petData.id, text: value })).then(
        (result) => {
          if (result.payload?.status !== 201) {
            createErrorToast({
              message: 'Something went wrong, please try again later',
            });
            return;
          }

          if (result?.payload?.status === 201) {
            setValue('');
          }
        }
      );
    }
  };

  const handleChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    setValue(event.target.value);
  };

  return (
    <div className={s.addComment}>
      <div className={s.avatar}>
        <Avatar src={`${User.getUser().image}_48x48.jpg`} />
      </div>
      <div className={s.input}>
        <Input
          label="Add your comment"
          name="comment"
          multiline
          rows={5}
          className="comment"
          onChange={handleChange}
          value={value}
        />
      </div>
      <RoundedButton
        onClick={handleComment}
        disabled={
          petCommentsData.loadingMoreStatus === LoadingStatusEnum.LOADING ||
          petCommentsData.status === LoadingStatusEnum.LOADING ||
          petCommentsData.isCommenting === LoadingStatusEnum.LOADING
        }
        className={s.send}
      >
        <SendIcon />
      </RoundedButton>
    </div>
  );
};

export default CommentForm;
