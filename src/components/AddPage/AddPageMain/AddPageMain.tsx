import React from 'react';
import s from './AddPageMain.module.scss';
import PetAddMedia from '../../common/PetAddMedia';
import AddPageForm from '../AddPageForm';

const AddPageMain = () => {
  return (
    <div className={s.root}>
      <PetAddMedia />
      <div className={s.addPageForm}>
        <AddPageForm />
      </div>
    </div>
  );
};

export default AddPageMain;
