import React, { useState } from 'react';
import s from './AddPageContest.module.scss';
import { Leaderboard } from '../../icons';
import { Typography } from '@material-ui/core';
import { Checkbox } from '../../ui/Checkbox';

export interface AddPageContestType {
  value: boolean;
  changeValue: (value: boolean) => void;
}

const AddPageContest: React.FC<AddPageContestType> = ({
  changeValue,
  value,
}) => {
  const handleChange = () => {
    changeValue(!value);
  };

  return (
    <div className={s.root}>
      <div className={s.contestPreview}>
        <span>
          <Leaderboard />
        </span>
      </div>
      <div className={s.contestInfo}>
        <Typography variant="body1">Dog contest in United States</Typography>
        <Typography variant="caption">
          From december 26 To january 16
        </Typography>
        <Checkbox
          name="participate"
          label="Participate in the contest"
          onChange={handleChange}
          value={value}
        />
      </div>
    </div>
  );
};

export default AddPageContest;
