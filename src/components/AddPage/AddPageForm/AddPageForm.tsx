import React, { useState, useEffect } from 'react';
import s from './AddPageForm.module.scss';
import FormikSelect from '../../ui/FormikSelect';
import { Typography } from '@material-ui/core';
import { Radio } from '../../ui/Radio';
import FormikRadioGroup from '../../ui/FormikRadioGroup';
import Button from '../../ui/Button';
import AddPageContest from '../AddPageContest';
import * as Yup from 'yup';
import { shallowEqual } from 'lib/utils/common';
import { Form, Formik } from 'formik';
import { useSelector } from 'store/store';
import { myPetsDataSelector, myPetsBreedsData } from 'store/selectors/myPets';
import FormikInput from '../../ui/FormikInput';
import { createErrorToast, createSuccessToast } from 'lib/utils/toasts';
import MainApiProtected from 'api/MainApiProtected';
import { useHistory } from 'react-router';
import { createFormData } from 'lib/utils/form-data';

const AddPageForm = () => {
  const [bool, setBool] = useState<boolean>(false);
  const myPetData = useSelector(myPetsDataSelector);
  const petBreeds = useSelector(myPetsBreedsData);
  const history = useHistory();

  const file: any = history.location.state;

  const [initialValues, setInitialValues] = useState({
    petName: '',
    petType: 'dog',
    breed: '',
    bio: '',
    country: '',
  });

  useEffect(() => {
    setInitialValues((values) => ({
      ...values,
    }));
  }, [myPetData]);

  const handleContest = (value: boolean) => {
    setBool(value);
  };

  const validationSchema = Yup.object({
    petName: Yup.string()
      .min(2, 'Minimum 2 symbols')
      .max(60, 'Maximum 60 symbols')
      .required('Required field'),
    bio: Yup.string()
      .min(10, 'Minimum 10 symbols')
      .max(1000, 'Maximum 1000 symbols'),
    breed: Yup.string().required('Select Required'),
    petType: Yup.string().required('Select Required'),
  });

  const handleSubmit = async (values: any) => {
    try {
      const result = await MainApiProtected.getInstance().postMyPet(
        createFormData({
          ...values,
          image: file,
          participate: Number(bool),
          fbLink: 'https://www.facebook.com/paw.voters/',
          instaLink: 'https://www.instagram.com/paw.voters/',
        })
      );

      if (result.status !== 201) {
        throw new Error('Something went wrong, please try again');
      }

      createSuccessToast({
        message: 'Your pet has been published successfully',
      });

      history.push('/pets');
    } catch (error) {
      if (error.data.errors.image) {
        createErrorToast({ message: `${error.data.errors.image[0]}` });
      }

      return error;
    }
  };

  return (
    <Formik
      enableReinitialize
      initialValues={initialValues}
      onSubmit={handleSubmit}
      validationSchema={validationSchema}
    >
      {({ values, initialValues, isValid, isSubmitting, setFieldValue }) => (
        <Form className={s.root}>
          {/* <div className={s.bottomed}>
            <FormikSelect
              label="Country"
              name="country"
              value={values.country}
              items={[
                { id: 0, petType: 'Country 1', name: 'da' },
                { id: 1, petType: 'Country 2', name: 'net' },
              ]}
            />
          </div> */}
          <Typography variant="h4" className={s.informationTitle}>
            Information about your pet
          </Typography>
          <FormikInput label="Pet Name *" name="petName" />
          <div className={s.radioGroupWrapper}>
            <Typography variant="body1" className={s.petTypeTitle}>Pet's type</Typography>
            <FormikRadioGroup
              aria-label="petType"
              name="petType"
              value={values.petType}
              className={s.radioGroup}
              setFieldValue={setFieldValue}
            >
              <Radio value="dog" label="Dog" />
              <Radio value="cat" label="Cat" />
            </FormikRadioGroup>
          </div>
          <div className={s.bottomed}>
            <FormikSelect
              label="Breed *"
              value={values.breed}
              name="breed"
              items={petBreeds}
            />
          </div>
          <div className={s.bottomed}>
            <FormikInput label="Bio" name="bio" multiline rows="6" />
          </div>
          <div className={s.bottomed}>
            <AddPageContest changeValue={handleContest} value={bool} />
          </div>
          <Button
            className={s.submitBtn}
            type="submit"
            disabled={
              shallowEqual(values, initialValues) ||
              !isValid ||
              isSubmitting ||
              !file
            }
          >
            Publish
          </Button>
        </Form>
      )}
    </Formik>
  );
};

export default AddPageForm;
