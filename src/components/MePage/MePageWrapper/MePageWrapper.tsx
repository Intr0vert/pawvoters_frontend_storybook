import React, { useEffect, useState } from 'react';
import s from './MePageWrapper.module.scss';
import { EditIcon, LoginSignupIcon, Close } from '../../icons';
import { Typography } from '@material-ui/core';
import MePageLinked from '../MePageLinked';
import Button from '../../ui/Button';
import { useAppDispatch, useSelector } from '../../../store/store';
import { finishRegistrationSelector } from '../../../store/selectors';
import MainApi from '../../../api/MainApi';
import { setUser } from '../../../store/slices/user';
import User from '../../../lib/User';
import {
  createErrorToast,
  createSuccessToast,
} from '../../../lib/utils/toasts';
import { useHistory } from 'react-router-dom';
import { setRegisterFinishInfo } from 'store/slices/app';
import * as Yup from 'yup';
import { Form, Formik } from 'formik';
import FormikInput from '../../ui/FormikInput';
import FormikCheckbox from '../../ui/FormikCheckbox';
import { getUserDataSelector } from '../../../store/selectors/user';
import { shallowEqual } from '../../../lib/utils/common';
import MainApiProtected from '../../../api/MainApiProtected';

const MePageWrapper = () => {
  const history = useHistory();
  const dispatch = useAppDispatch();
  const finishRegistrationData = useSelector(finishRegistrationSelector);
  const userData = useSelector(getUserDataSelector);

  const [initialValues, setInitialValues] = useState(() => ({
    nickname: userData?.nickname || '',
    city: userData?.city || '',
    email: finishRegistrationData.userEmail || userData?.email || '',
    voteMyPost: userData?.emailNotificationSettings.voteMyPost !== false,
    commentPostWithMyVote:
      userData?.emailNotificationSettings.commentPostWithMyVote !== false,
    commentMyPost: userData?.emailNotificationSettings.commentMyPost !== false,
    replyMyComment:
      userData?.emailNotificationSettings.replyMyComment !== false,
  }));

  useEffect(() => {
    setInitialValues({
      nickname: userData?.nickname || '',
      city: userData?.city || '',
      email: finishRegistrationData.userEmail || userData?.email || '',
      voteMyPost: userData?.emailNotificationSettings.voteMyPost !== false,
      commentPostWithMyVote:
        userData?.emailNotificationSettings.commentPostWithMyVote !== false,
      commentMyPost:
        userData?.emailNotificationSettings.commentMyPost !== false,
      replyMyComment:
        userData?.emailNotificationSettings.replyMyComment !== false,
    });
  }, [userData]);

  const validationSchema = Yup.object({
    nickname: Yup.string()
      .min(2, 'Minimum 2 symbols')
      .max(60, 'Maximum 60 symbols')
      .required('Required field'),
    city: Yup.string()
      .min(2, 'Minimum 2 symbols')
      .max(255, 'Maximum 255 symbols')
      .required('Required field'),
    email: Yup.string()
      .min(6, 'Minimum 6 symbols')
      .max(255, 'Maximum 255 symbols')
      .matches(
        /^[a-z0-9!#$%&'*+\=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+\=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)(?=.*[a-z])(?=.*[a-zA-Z])([a-zA-Z0-9]+){2}$/i,
        'Wrong email'
      )
      .required('Required field'),
  });

  const handleSubmit = async (values: any) => {
    if (finishRegistrationData.active) {
      const result = await MainApi.getInstance().postUsers({
        ...values,
        userToken: finishRegistrationData.userToken,
        userEmail: finishRegistrationData.userEmail,
        emailNotificationSettings: {
          voteMyPost: values.voteMyPost,
          commentPostWithMyVote: values.commentPostWithMyVote,
          commentMyPost: values.commentMyPost,
          replyMyComment: values.replyMyComment,
        },
      });

      if (result.status !== 201) {
        createErrorToast({
          message: 'Something went wrong, please try again',
        });

        return;
      }

      if (result.status === 201) {
        dispatch(setUser(result.data));
        User.setUser(result.data);

        createSuccessToast({ message: 'You have been logged in successfully' });
        history.push('/pets');

        dispatch(
          setRegisterFinishInfo({
            userEmail: null,
            userToken: null,
            active: false,
          })
        );
      }

      return;
    }

    const result = await MainApiProtected.getInstance().postUsersMe({
      nickname: values.nickname,
      city: values.city,
      emailNotificationSettings: {
        voteMyPost: values.voteMyPost,
        commentPostWithMyVote: values.commentPostWithMyVote,
        commentMyPost: values.commentMyPost,
        replyMyComment: values.replyMyComment,
      },
    });

    if (result.status !== 200) {
      createErrorToast({
        message: 'Something went wrong, please try again',
      });

      return;
    }

    dispatch(setUser(result.data));
    User.setUser(result.data);
    createSuccessToast({ message: 'Your account was updated' });
  };

  return (
    <div className={s.root}>
      <div className={s.row}>
        <div className={s.avatarCol}>
          <div className={s.avatar}>
            <LoginSignupIcon />
          </div>
        </div>
        <div className={s.formCol}>
          <Formik
            enableReinitialize
            initialValues={initialValues}
            onSubmit={handleSubmit}
            validationSchema={validationSchema}
          >
            {({ values, initialValues, isValid, isSubmitting }) => (
              <Form>
                <Typography variant="h4" className={s.whoOwner}>
                  Who is the owner of the pet?
                </Typography>
                <div className={s.nickname}>
                  <FormikInput
                    label="Owner name*"
                    name="nickname"
                    wrapperClass={s.bottomed}
                  />
                </div>
                <div className={s.city}>
                  <FormikInput label="City" name="city" />
                </div>
                <Typography variant="h4" className={s.loginInfo}>
                  Login information
                </Typography>
                <div className={s.email}>
                  <FormikInput
                    label="Email"
                    name="email"
                    type="email"
                    disabled
                    wrapperClass={s.emailBlock}
                  />
                </div>
                <button
                  type="button"
                  className={s.changeEmail}
                  disabled={finishRegistrationData.active}
                >
                  <EditIcon />
                  Change email
                </button>
                {!finishRegistrationData.active && <MePageLinked />}
                <Typography variant="h4" className={s.emailNotifications}>
                  Email notification settings
                </Typography>
                <div className={s.checkboxGroup}>
                  <div className={s.checkboxGroupCol}>
                    <FormikCheckbox
                      label="Votes on my pets"
                      name="voteMyPost"
                    />
                    <FormikCheckbox
                      label="Comment on posts"
                      name="commentPostWithMyVote"
                    />
                  </div>
                  <div className={s.checkboxGroupCol}>
                    <FormikCheckbox
                      label="Contest email"
                      name="commentMyPost"
                    />
                    <FormikCheckbox
                      label="Replies to comments I made"
                      name="replyMyComment"
                    />
                  </div>
                </div>
                <Button
                  type="submit"
                  disabled={
                    shallowEqual(values, initialValues) ||
                    !isValid ||
                    isSubmitting
                  }
                >
                  Save
                </Button>
              </Form>
            )}
          </Formik>
        </div>
      </div>
    </div>
  );
};

export default MePageWrapper;
