import React from 'react';
import s from './Banner.module.scss';
import cn from 'classnames';
import { Typography } from '@material-ui/core';
import Button from '../../ui/Button';
import { Add, HomeBannerImage, BannerDecoration } from '../../icons';
import { Link } from 'react-router-dom';
import { useAppDispatch } from 'store/store';
import { setModalLoginOpened } from 'store/slices/app';
import { useUser } from '../../../lib/hooks/useUser';

const Banner = () => {
  const dispatch = useAppDispatch();
  const { isAuth } = useUser();

  const handleButtonClick = (
    e: React.MouseEvent<HTMLAnchorElement, MouseEvent>
  ) => {
    e.preventDefault();
    dispatch(setModalLoginOpened({ opened: true }));
  };

  return (
    <div className={cn(s.root)}>
      <div className={cn(s.bannerDecoration)}>
        <BannerDecoration />
      </div>
      <div className={cn(s.main)}>
        <Typography variant="h1">Favourite place for your pet</Typography>
        <div className={cn(s.controls)}>
          <Link to="/add">
            <Button className={cn(s.addPetButton)} color="secondary">
              <span>
                <Add />
                Add pet
              </span>
            </Button>
          </Link>
          {!isAuth && (
            <a href="#" className={cn(s.loginLink)} onClick={handleButtonClick}>
              Login / Sign up
            </a>
          )}
        </div>
      </div>
      <div className={cn(s.image)}>
        <HomeBannerImage />
      </div>
    </div>
  );
};

export default Banner;
