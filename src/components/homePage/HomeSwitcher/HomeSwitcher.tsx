import React, { useEffect } from 'react';
import cn from 'classnames';
import s from './HomeSwitcher.module.scss';
import TabSwitcher from 'components/ui/TabSwitcher';
import { useAppDispatch, useSelector } from '../../../store/store';
import { homePetsSelector } from '../../../store/selectors';
import { setOrder } from '../../../store/slices/home';
import { LoadingStatusEnum } from '../../../types/enum/LoadingStatus.enum';
import { useHistory, useLocation } from 'react-router-dom';
import { TabItem } from '../../ui/TabSwitcher/TabSwitcher';
import { parse } from 'query-string';

const switcherElements = [
  { id: 0, label: 'Popular', value: 'popular' },
  { id: 1, label: 'Recent', value: 'recent' },
];

const HomeSwitcher = () => {
  const dispatch = useAppDispatch();
  const pets = useSelector(homePetsSelector);
  const history = useHistory();
  const location = useLocation();

  const handleChange = (item: TabItem) => {
    const order = item.id === 0 ? 'popular' : 'recent';

    history.push(item.value === 'popular' ? '/' : `/?order=${order}`);
  };

  useEffect(() => {
    const parsedQuery = parse(location.search);
    const order = parsedQuery.order as 'popular' | 'recent' | undefined;

    if (order === 'recent') {
      dispatch(setOrder(order));
    } else {
      dispatch(setOrder('popular'));
    }
  }, []);

  return (
    <div className={cn(s.switcherBlock)}>
      <TabSwitcher
        items={switcherElements}
        disabled={pets.status === LoadingStatusEnum.LOADING}
        onChange={handleChange}
        active={
          pets.order === 'popular' ? switcherElements[0] : switcherElements[1]
        }
      />
    </div>
  );
};

export default HomeSwitcher;
