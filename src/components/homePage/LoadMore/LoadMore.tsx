import { Container } from '@material-ui/core';
import Button from 'components/ui/Button';
import React, { memo, useEffect } from 'react';
import { useInView } from 'react-intersection-observer';
import { homePetsSelector } from 'store/selectors';
import { fetchHomePagePets } from 'store/slices/home/thunks';
import { useAppDispatch, useSelector } from 'store/store';
import { LoadingStatusEnum } from 'types/enum/LoadingStatus.enum';
import s from './LoadMore.module.scss';
import { LoadMore as LoadMoreIcon } from 'components/icons';

const LoadMore = () => {
  const dispatch = useAppDispatch();
  const pets = useSelector(homePetsSelector);
  const { ref, inView } = useInView({
    threshold: 0,
  });

  useEffect(() => {
    if (
      pets.status !== LoadingStatusEnum.LOADING &&
      pets.status !== LoadingStatusEnum.ERROR &&
      inView
    ) {
      dispatch(fetchHomePagePets({ page: pets.pageToLoad }));
    }
  }, [inView]);

  const handleLoadMore = () => {
    dispatch(fetchHomePagePets({ page: pets.pageToLoad }));
  };

  return (
    <div className={s.root}>
      {Boolean(pets.links.next) && (
        <>
          {pets.pageToLoad % 4 !== 0 && (
            <div ref={ref} className={s.observerMarker} />
          )}
          <Container className={s.morePetsContainer}>
            {!pets.firstLoad &&
              Boolean(pets.links.next) &&
              pets.pageToLoad % 4 === 0 && (
                <Button
                  disabled={pets.status === LoadingStatusEnum.LOADING}
                  onClick={handleLoadMore}
                >
                  <LoadMoreIcon />
                  Load more pets
                </Button>
              )}
          </Container>
        </>
      )}
    </div>
  );
};

export default memo(LoadMore);
