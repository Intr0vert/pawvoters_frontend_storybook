import React from 'react';
import Image from '../Image';
import s from './Avatar.module.scss';
import cn from 'classnames';
import { LoginSignupIcon } from 'components/icons';

interface Props {
  src: string;
  alt?: string;
}

const UnloaderComponent = (
  <span className={s.unloader}>
    <LoginSignupIcon />
  </span>
);

const Avatar: React.FC<Props & React.HTMLAttributes<HTMLImageElement>> = ({
  src,
  alt,
  className,
  ...rest
}) => {
  return (
    <div className={s.imageRoot}>
      <Image
        src={src}
        alt={alt}
        className={cn(s.root, className)}
        unloader={UnloaderComponent}
        {...rest}
      />
    </div>
  );
};

export default Avatar;
