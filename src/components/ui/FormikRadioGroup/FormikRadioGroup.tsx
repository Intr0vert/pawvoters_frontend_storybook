import React, { useEffect } from 'react';
import RadioGroup from '@material-ui/core/RadioGroup';
import { useField } from 'formik';
import { useAppDispatch } from 'store/store';
import { getBreeds } from 'store/slices/myPets/thunks';

interface FormikRadioGroup {
  name: string;
  value: string;
  setFieldValue?: Function;
}

export const FormikRadioGroup: React.FC<
  FormikRadioGroup & React.HTMLAttributes<HTMLInputElement>
> = ({ name, value, children, className, setFieldValue }) => {
  const [field, meta, helpers] = useField({ value, name });
  const dispatch = useAppDispatch();

  const handleChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    helpers.setValue(event.target.value);

    if (setFieldValue) {
      setFieldValue('breed', '');
    }
  };

  useEffect(() => {
    dispatch(getBreeds({ petType: field.value }));
  }, [field.value]);

  return (
    <RadioGroup
      name={name}
      value={value}
      onChange={handleChange}
      children={children}
      className={className}
    />
  );
};
