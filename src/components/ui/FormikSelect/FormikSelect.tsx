import React from 'react';
import { Select } from '../Select';
import { useField } from 'formik';

interface FormikSelect {
  label: string;
  name: string;
  items: any;
  value?: string;
}

export const FormikSelect: React.FC<
  FormikSelect & React.HTMLAttributes<HTMLInputElement>
> = ({ label, name, items, value }) => {
  const [field, meta, helpers] = useField(name);

  const handleChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    helpers.setValue(event.target.value);
  };

  return (
    <Select
      label={label}
      name={name}
      items={items}
      value={value}
      customOnChange={handleChange}
    />
  );
};
