import { makeStyles, createStyles, Theme } from '@material-ui/core';

export const useSliderStyles = makeStyles((theme: Theme) => createStyles({
  label: {
    position: 'absolute',
    width: 38,
    height: 54,
    top: -52,
    marginLeft: '-13px',
    transform: 'scale(0)',
    transformOrigin: 'bottom center',
    transition: 'transform 150ms cubic-bezier(0.4, 0, 0.2, 1) 0ms',
    '& i': {
      position: 'absolute',
      left: '50%',
      top: 15,
      color: '#fff',
      fontSize: 15,
      fontWeight: 600,
      transform: 'translate(-50%, 0)',
    },
    '&[data-open=true]': {
      visibility: 'visible',
      transform: 'scale(1)'
    },
    '& svg': {
      width: 38,
      height: 54,
    }
  },
}));


