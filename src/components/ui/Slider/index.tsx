import React from 'react';
import { Slider as MaterialSlider } from '@material-ui/core/';
import { useSliderStyles } from './styles';

export interface SliderProps {
  defaultValue: number | number[];
  step: number;
  min: number;
  max: number;
}

export const Slider: React.FC<SliderProps> = ({
  defaultValue,
  step,
  min,
  max,
}) => {
  const classes = useSliderStyles();
  return (
    <MaterialSlider
      defaultValue={defaultValue}
      step={step}
      min={min}
      max={max}
      valueLabelDisplay="auto"
      ValueLabelComponent={({ children, open, value }) => {
        return (
          <>
            {children}
            <span
              {...children.props}
              className={classes.label}
              data-open={open}
            >
              <svg
                width="39"
                height="54"
                viewBox="0 0 39 54"
                fill="none"
                xmlns="http://www.w3.org/2000/svg"
              >
                <path
                  d="M0.5 15C0.5 6.71573 7.21573 0 15.5 0H23.5C31.7843 0 38.5 6.71573 38.5 15V28.5974C38.5 34.1912 34.8646 39.1361 29.5254 40.8046C25.1762 42.1637 21.8786 45.7341 20.8688 50.1774L20 54L18.6069 49.6216C17.2864 45.4715 14.0245 42.2264 9.86764 40.9274C4.29457 39.1858 0.5 34.0245 0.5 28.1856V15Z"
                  fill="#E9B32A"
                />
              </svg>
              <i>{value}</i>
            </span>
          </>
        );
      }}
    />
  );
};
