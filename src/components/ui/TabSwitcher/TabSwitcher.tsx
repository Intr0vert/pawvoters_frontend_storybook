import React, { FC } from 'react';
import s from './TabSwitcher.module.scss';
import cn from 'classnames';

export type TabItem = {
  id: number;
  label: string;
  value: string;
};

export interface ITabsSwitcher {
  items: TabItem[];
  onChange: (item: TabItem) => void;
  disabled?: boolean;
  active: TabItem;
}

const TabSwitcher: FC<ITabsSwitcher> = ({
  items,
  onChange,
  disabled,
  active,
}) => {
  const handleClick = (item: TabItem) => () => {
    if (disabled) {
      return;
    }

    if (item.id === active.id) {
      return;
    }

    onChange(item);
  };

  return (
    <div className={cn(s.root, { [s.disabled]: disabled })}>
      <span
        className={cn(s.indicator, {
          [s.firstItem]: active.id === items[0].id,
          [s.secondItem]: active.id === items[1].id,
        })}
      />
      {items.map((item: TabItem) => (
        <span
          key={item.id}
          onClick={handleClick(item)}
          className={cn(s.tabItem, {
            [s.activeTab]: active.id === item.id,
          })}
        >
          {item.label}
        </span>
      ))}
    </div>
  );
};

export default TabSwitcher;
