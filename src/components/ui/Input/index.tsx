import React from 'react';
import TextField from '@material-ui/core/TextField';
import { useInputStyles } from './styles';
import cn from 'classnames';

export interface InputProps {
  label: string;
  name: string;
  type?: string;
  disabled?: boolean;
  placeholder?: string;
  id?: string;
  multiline?: boolean;
  rows?: string | number | undefined;
  error?: boolean;
  helperText?: string;
  success?: boolean;
  beginIcon?: React.ReactNode;
  endIcon?: React.ReactNode;
  className?: string;
  value?: string;
  onChange?: (event: React.ChangeEvent<HTMLInputElement>) => void;
}

export const Input: React.FC<
  InputProps & React.HTMLAttributes<HTMLInputElement>
> = ({
  label,
  type = 'text',
  disabled,
  placeholder,
  name,
  id,
  multiline,
  rows,
  error,
  helperText,
  success,
  beginIcon,
  endIcon,
  className,
  value,
  onChange,
}) => {
  const classes = useInputStyles();

  return (
    <div className={classes.wrapper}>
      {beginIcon}
      <TextField
        variant="filled"
        classes={{
          root: classes.root,
        }}
        className={cn({
          success,
          multiline,
          startAdornment: beginIcon,
        })}
        label={label}
        type={type}
        disabled={disabled}
        placeholder={placeholder}
        name={name}
        id={id || name}
        multiline={multiline}
        rows={rows}
        error={error}
        helperText={error && helperText}
        onChange={onChange}
        InputProps={{
          endAdornment: endIcon,
          className,
          value,
          onChange,
        }}
      />
    </div>
  );
};
