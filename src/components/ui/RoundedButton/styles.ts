import { makeStyles, createStyles, Theme } from '@material-ui/core';

export const useRoundedButtonStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      padding: '0',
      background: 'rgba(235, 134, 75, 0.2)',
      '&:hover': {
        background: 'rgba(235, 134, 75, 0.25)',
      },
      '&:disabled': {
        background: 'rgba(226, 235, 248, 0.2)',
      },
    },
  })
);
