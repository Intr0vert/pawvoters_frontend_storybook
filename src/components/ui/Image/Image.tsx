import React from 'react';
import { Img } from 'react-image';
import s from './Image.module.scss';
import cn from 'classnames';

interface ImageI {
  src: string | string[];
  alt?: string | undefined;
  loader?: JSX.Element | null | undefined;
  unloader?: JSX.Element | null | undefined;
}

const UnloaderComponent = <span className={cn(s.unloader)} />;

const LoaderComponent = <span className={cn(s.loader)} />;

const Image: React.FC<ImageI & React.HTMLAttributes<HTMLImageElement>> = ({
  src,
  alt,
  unloader = UnloaderComponent,
  loader = LoaderComponent,
  ...rest
}) => <Img src={src} alt={alt} unloader={unloader} loader={loader} {...rest} />;

export default Image;
