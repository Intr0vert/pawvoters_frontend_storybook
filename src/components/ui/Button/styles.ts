import { makeStyles, createStyles, Theme } from '@material-ui/core';

export const useButtonStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      fontSize: 14,
      minWidth: 170,
      height: 55,
      textTransform: 'initial',
      borderRadius: 15,
      boxShadow: 'none',
      padding: '0 16px',
      '&:focus': {
        boxShadow: 'none',
      },
      '& span': {
        display: 'flex',
        alignItems: 'center',
        '& svg': {
          marginRight: 10,
        },
      },
    },
    outlined: {
      background: theme.palette.background.default,
      borderWidth: 2,
      '&:hover': {
        borderWidth: 2,
        borderColor: theme.palette.secondary.main,
        backgroundColor: 'rgba(255, 215, 111, 0.1)',
      },
      '&:disabled': {
        backgroundColor: 'background: rgba(226, 235, 248, 0.2)',
        borderColor: '#A4AFBE',
        borderWidth: 2,
        color: '#A4AFBE',
      },
    },
    contained: {
      color: '#fff',
      '&:disabled': {
        backgroundColor: '#F4F9FE',
        color: '#A4AFBE',
      },
      '&:hover': {
        boxShadow: 'none',
        backgroundColor: theme.palette.primary.dark,
      },
    },
    containedSizeSmall: {
      height: 40,
    },
    label: {
      fontSize: 14,
    },
  })
);
