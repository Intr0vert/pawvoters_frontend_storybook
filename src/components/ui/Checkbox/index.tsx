import React from 'react';
import {
  Checkbox as MaterialCheckbox,
  FormControlLabel,
} from '@material-ui/core';
import { useCheckboxStyles } from './styles';

export interface CheckboxProps {
  label: string;
  disabled?: boolean;
  name: string;
  value: boolean;
}

export const Checkbox: React.FC<
  CheckboxProps & React.HTMLAttributes<HTMLInputElement>
> = ({ label, disabled, name, onChange, value }) => {
  const classes = useCheckboxStyles();

  return (
    <FormControlLabel
      name={name}
      disabled={disabled}
      classes={{
        label: classes.label,
      }}
      control={
        <MaterialCheckbox
          checked={value}
          onChange={onChange}
          color="primary"
          classes={{
            root: classes.root,
          }}
        />
      }
      label={label}
    />
  );
};
