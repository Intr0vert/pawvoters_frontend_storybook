import { makeStyles, createStyles, Theme } from '@material-ui/core';

export const useCheckboxStyles = makeStyles((theme: Theme) => createStyles({
  root: {
    '&.Mui-disabled': {
      color: '#A4AFBE',
    }
  },
  label: {
    '&.Mui-disabled': {
      color: '#31344E',
    }
  }
}));


