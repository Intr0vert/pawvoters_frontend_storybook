import React from 'react';
import TextField from '@material-ui/core/TextField';
import { useInputStyles } from '../Input/styles';
import cn from 'classnames';
import { InputProps } from '../Input';
import { useField } from 'formik';

interface FormikInputProps {
  customChange?: (
    setValue: (value: any, shouldValidate?: boolean | undefined) => void,
    value: any
  ) => void;
  customFocus?: (
    setValue: (value: any, shouldValidate?: boolean | undefined) => void,
    value: any
  ) => void;
  customBlur?: (
    setValue: (value: any, shouldValidate?: boolean | undefined) => void,
    value: any
  ) => void;
  wrapperClass?: string;
}

export const FormikInput: React.FC<
  InputProps & FormikInputProps & React.HTMLAttributes<HTMLInputElement>
> = ({
  label,
  type = 'text',
  disabled,
  placeholder,
  name,
  id,
  multiline,
  rows,
  success,
  beginIcon,
  endIcon,
  className,
  customChange,
  customBlur,
  customFocus,
  wrapperClass,
}) => {
  const classes = useInputStyles();
  const [field, meta, helpers] = useField({ name });

  const inputHandleChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    if (name?.toString() === 'email') {
      helpers.setValue(e.target.value.replace(/\s/g, ''));
      return;
    }
    if (customChange) {
      customChange(helpers.setValue, e.target.value);
      return;
    }
    field.onChange(e);
  };

  const inputHandleFocus = (e: React.FocusEvent<HTMLInputElement>) => {
    if (type !== 'password') {
      helpers.setValue(e.target.value?.trim());
    }
    if (customFocus) {
      customFocus(helpers.setValue, e.target.value);
    }
  };

  const inputHandleBlur = (e: React.FocusEvent<HTMLInputElement>) => {
    field.onBlur(e);
    let { value: localValue } = e.target;

    if (type !== 'password') {
      localValue = localValue.trim();
    }

    if (customBlur) {
      customBlur(helpers.setValue, localValue);
      return;
    }

    helpers.setValue(localValue);
  };

  return (
    <div className={cn(classes.wrapper, wrapperClass)}>
      {beginIcon}
      <TextField
        variant="filled"
        classes={{
          root: classes.root,
        }}
        className={cn({
          success,
          multiline,
          startAdornment: beginIcon,
        })}
        label={label}
        type={type}
        disabled={disabled}
        placeholder={placeholder}
        id={id || name}
        multiline={multiline}
        rows={rows}
        error={Boolean(meta.touched && meta.error)}
        helperText={meta.touched && meta.error}
        InputProps={{
          endAdornment: endIcon,
          className,
          ...field,
          onChange: inputHandleChange,
          onFocus: inputHandleFocus,
          onBlur: inputHandleBlur,
        }}
      />
    </div>
  );
};
