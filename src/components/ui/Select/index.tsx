import React from 'react';
import TextField from '@material-ui/core/TextField';
import { useInputStyles } from '../Input/styles';
import { MenuItem } from '@material-ui/core';
import classNames from 'classnames';

export interface SelectItemProps {
  placeholder: string;
  value: string;
  id: number | string;
  petType?: string;
  name?: string;
}

export interface SelectProps {
  label: string;
  disabled?: boolean;
  name: string;
  id?: string;
  error?: boolean;
  helperText?: string;
  items: SelectItemProps[];
  value?: string;
  customOnChange?: (event: React.ChangeEvent<HTMLInputElement>) => void;
}

export const Select: React.FC<SelectProps> = ({
  disabled,
  name,
  id,
  value,
  error,
  helperText,
  customOnChange,
  items,
  label,
}) => {
  const classes = useInputStyles();

  const handleChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    if (customOnChange) {
      customOnChange(event);
    }
  };

  return (
    <TextField
      className={classNames('select')}
      fullWidth
      variant="filled"
      classes={{
        root: classes.root,
      }}
      disabled={disabled}
      name={name}
      id={id || name}
      select
      error={error}
      helperText={error && helperText}
      value={value}
      onChange={handleChange}
      label={!value && label}
      SelectProps={{
        displayEmpty: false,
        MenuProps: {
          anchorOrigin: {
            vertical: 'bottom',
            horizontal: 'left',
          },
          transformOrigin: {
            vertical: 'top',
            horizontal: 'left',
          },
          getContentAnchorEl: null,
          classes: {
            paper: classes.selectRoot,
            list: classes.selectList,
          },
        },
        IconComponent: () => (
          <svg
            width="16"
            height="16"
            viewBox="0 0 16 16"
            fill="none"
            xmlns="http://www.w3.org/2000/svg"
          >
            <mask
              id="mask0"
              mask-type="alpha"
              maskUnits="userSpaceOnUse"
              x="0"
              y="0"
              width="16"
              height="16"
            >
              <path
                d="M7.99999 9.99997L5.17133 7.1713L6.11466 6.22864L7.99999 8.11464L9.88533 6.22864L10.8287 7.1713L7.99999 9.99997Z"
                fill="#31344E"
              />
            </mask>
            <g mask="url(#mask0)">
              <rect width="16" height="16" fill="#A4AFBE" />
            </g>
          </svg>
        ),
      }}
    >
      {items.map((option) => (
        <MenuItem key={option.id} value={option.value || option.name}>
          {option.placeholder || option.name}
        </MenuItem>
      ))}
    </TextField>
  );
};
