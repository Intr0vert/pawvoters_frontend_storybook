import React from 'react';
import s from './PetAddImage.module.scss';
import EditIcon from '../../icons/Edit';

export interface PetImage {
  img?: string;
  getImage: (event: React.ChangeEvent<HTMLInputElement>) => void;
}

const PetImage: React.FC<PetImage> = ({ img, getImage }) => (
  <>
    <div className={s.editAvatar}>
      <input
        name="file"
        type="file"
        className={s.inputZone}
        onChange={getImage}
      />
      <EditIcon />
    </div>
    <img className={s.petAvatar} src={img} />
  </>
);

export default PetImage;
