import { Typography } from '@material-ui/core';
import React, { useState } from 'react';
import s from './LoginModal.module.scss';
import { Input } from 'components/ui/Input';
import Button from 'components/ui/Button';
import { FacebookModalIcon, MailIcon } from 'components/icons';
import { GoogleIcon } from 'components/icons/';
import SpringModal from '../SpringModal';
import { useAppDispatch, useSelector } from 'store/store';
import { setModalLoginOpened } from 'store/slices/app';
import { loginModalSelector } from 'store/selectors';
import {
  GoogleLogin,
  GoogleLoginResponse,
  GoogleLoginResponseOffline,
} from 'react-google-login';
import MainApi from 'api/MainApi';
import { UserAuthEnum } from 'types/enum/UserAuth.enum';
import { setUser } from 'store/slices/user';
import { createErrorToast } from 'lib/utils/toasts';
// @ts-ignore
import FacebookLogin from 'react-facebook-login/dist/facebook-login-render-props';
import { ReactFacebookLoginInfo } from 'react-facebook-login';
import User from 'lib/User';

const VoteForPetModal = () => {
  const modalState = useSelector(loginModalSelector);
  const dispatch = useAppDispatch();
  const [email, setEmail] = useState('');
  const [emailSent, setEmailSent] = useState(false);

  const handleClose = () => {
    dispatch(setModalLoginOpened({ opened: false }));
  };

  const handleSuccessGoogleResponse = async (
    response: GoogleLoginResponse | GoogleLoginResponseOffline
  ) => {
    if ('accessToken' in response) {
      const result = await MainApi.getInstance().postLogin(
        UserAuthEnum.GOOGLE,
        response.accessToken
      );

      if (result.status === 200) {
        dispatch(setUser(result.data));
        User.setUser(result.data);
        handleClose();
      }
    }
  };

  const handleFailureGoogleResponse = (error: any) => {
    console.log(error);
    if (error.error === 'popup_closed_by_user') {
      createErrorToast({
        message: 'Popup was close before finishing the sign in',
      });
      return;
    }

    if (error.error === 'access_denied') {
      createErrorToast({
        message: 'Not enough permissions to access this account',
      });
      return;
    }

    createErrorToast({
      message: 'Something went wrong, please try again later',
    });
  };

  const handleSuccessFacebookResponse = async (
    response: ReactFacebookLoginInfo
  ) => {
    console.log(response);
    if ('accessToken' in response) {
      const result = await MainApi.getInstance().postLogin(
        UserAuthEnum.FACEBOOK,
        response.accessToken
      );

      if (result.status === 200) {
        dispatch(setUser(result.data));
        User.setUser(result.data);
        handleClose();
      }
    }
  };

  const handleSubmit = async (event: React.FormEvent<HTMLFormElement>) => {
    event.preventDefault();
    const { status, data } = await MainApi.getInstance().postLoginInit(email);

    if (status === 200 && 'token' in data) {
      const {
        status: postLoginStatus,
        data: postLoginData,
      } = await MainApi.getInstance().postLogin(
        UserAuthEnum.CUSTOM,
        data.token
      );

      if (postLoginStatus === 200) {
        dispatch(setUser(postLoginData));
        User.setUser(postLoginData);
        handleClose();
      }

      return;
    }

    if (status === 200 || status === 201) {
      console.log(data);
      console.log('201');
      setEmailSent(true);
    }

    if (status !== 200 && status !== 201) {
      createErrorToast({
        message: 'Something went wrong, please try again later',
      });
    }
  };

  return (
    <SpringModal opened={modalState.opened} handleClose={handleClose}>
      <div className={s.root}>
        <Typography variant="h3" className={s.title}>
          Sing in
        </Typography>
        {emailSent ? (
          <div>Email sent</div>
        ) : (
          <>
            <div className={s.social}>
              <GoogleLogin
                clientId="537498204820-584fpmc4ljk82sakhl4vdu75gafjijsl.apps.googleusercontent.com"
                render={(renderProps) => (
                  <button
                    onClick={renderProps.onClick}
                    disabled={renderProps.disabled}
                  >
                    <i>
                      <GoogleIcon />
                    </i>
                  </button>
                )}
                onSuccess={handleSuccessGoogleResponse}
                onFailure={handleFailureGoogleResponse}
              />

              <FacebookLogin
                appId="2857841794495673"
                callback={handleSuccessFacebookResponse}
                render={(renderProps: {
                  onClick:
                    | ((
                        event: React.MouseEvent<HTMLButtonElement, MouseEvent>
                      ) => void)
                    | undefined;
                }) => (
                  <button onClick={renderProps.onClick}>
                    <i>
                      <FacebookModalIcon />
                    </i>
                  </button>
                )}
              />
            </div>
            <Typography variant="body1" color="textSecondary">
              or use your email account
            </Typography>
            <form onSubmit={handleSubmit}>
              <Input
                beginIcon={<MailIcon />}
                label="Email"
                name="email"
                type="email"
                value={email}
                onChange={(e: React.ChangeEvent<HTMLInputElement>) => {
                  setEmail(e.target.value);
                }}
              />
              <Button className={s.submitBtn} type="submit">
                Send
              </Button>
            </form>
          </>
        )}
      </div>
    </SpringModal>
  );
};

export default VoteForPetModal;
