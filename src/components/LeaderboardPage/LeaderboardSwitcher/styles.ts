import { createStyles, makeStyles, Theme } from '@material-ui/core';

export const useButtonStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      width: 154,
      borderRadius: 30,
      padding: 0,
      '&:hover': {
        background: 'none',
      },
      '&.active': {
        background: 'var(--primary)',
        '&:hover': {
          background: 'var(--primary-dark)',
        },
      },
      '&.active span': {
        fontWeight: 500,
        color: 'white',
      },
    },
    label: {
      padding: '10px 0',
      textTransform: 'capitalize',
      color: 'rgba(0, 0, 0, 0.6)',
      fontWeight: 600,
      fontSize: 18,
      '&:hover': {
        color: 'var(--primary)',
      },
    },
  })
);
