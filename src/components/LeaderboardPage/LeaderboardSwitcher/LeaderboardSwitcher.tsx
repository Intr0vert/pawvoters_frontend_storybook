import React from 'react';
import s from './LeaderboardSwitcher.module.scss';

const switcherElements = [
  { id: 0, label: 'Dogs', value: 'popular' },
  { id: 1, label: 'Cats', value: 'recent' },
  { id: 2, label: 'Winners', value: 'recent' },
];

const LeaderboardSwitcher = () => {
  return <div className={s.switcher}>1</div>;
};

export default LeaderboardSwitcher;

// {sortingButton.map((item, id) => {
//   return (
//     <Button
//       onClick={() => handleRederect(item)}
//       color="primary"
//       classes={{
//         root: classes.root,
//         label: classes.label,
//       }}
//       className={item.type === search.slice(1) ? 'active' : ''}
//       key={id}
//     >
//       {item.placeholder}
//     </Button>
//   );
// })}
