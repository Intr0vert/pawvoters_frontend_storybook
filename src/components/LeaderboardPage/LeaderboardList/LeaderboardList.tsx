import React from 'react';
import { useLocation } from 'react-router-dom';
import { Cats, Dogs, Winners } from './ListTypes';
import s from './LeaderboardList.module.scss';

const LeaderboardList = () => {
  const { search } = useLocation();

  const Lists = () => {
    switch (search) {
      case '?type=cats':
        <Cats />;
        break;
      case '?type=winners':
        <Winners />;
        break;
    }

    return <Dogs />;
  };

  return (
    <div className={s.root}>
      <Lists />
    </div>
  );
};

export default LeaderboardList;
