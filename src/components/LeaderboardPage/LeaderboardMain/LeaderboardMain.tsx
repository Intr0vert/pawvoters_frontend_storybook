import React from 'react';
import s from './LeaderboardMain.module.scss';
import LeaderboardList from '../LeaderboardList';
import LeaderboardSwitcher from '../LeaderboardSwitcher';

const LeaderboardMain = () => {
  return (
    <div className={s.root}>
      <LeaderboardSwitcher />
      <LeaderboardList />
    </div>
  );
};

export default LeaderboardMain;
