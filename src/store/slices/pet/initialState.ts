import { LoadingStatusEnum } from 'types/enum/LoadingStatus.enum';
import { PetType } from 'types/pet/Pet.type';
import { PetVoteItemType } from 'types/pet/PetVoteItem.type';
import { CommentItemType } from 'types/pet/CommentItem.type';
import { PaginatedMeta } from 'types/interfaces/common/PaginatedMeta.interface';
import { PaginatedLinks } from 'types/interfaces/common/PaginatedLinks.interface';
import { HomePost } from 'types/interfaces/HomePost.interface';
import { ReportItemType } from '../../../types/pet/ReportItem.type';

type PetData = {
  error: any;
  data: PetType | null;
  status: LoadingStatusEnum;
};

type PetVotesData = {
  error: any;
  data: PetVoteItemType[];
  status: LoadingStatusEnum;
};

type PetCommentsData = {
  error: any;
  data: CommentItemType[];
  meta: PaginatedMeta;
  links: PaginatedLinks;
  status: LoadingStatusEnum;
  loadingMoreStatus: LoadingStatusEnum;
  isCommenting: LoadingStatusEnum;
  isDeleting: LoadingStatusEnum;
  isMore: boolean;
  pageToLoad: number;
};

type SimilarPetsData = {
  error: any;
  data: HomePost[];
  status: LoadingStatusEnum;
};

type ReportReasons = {
  error: any;
  data: ReportItemType[];
  status: LoadingStatusEnum;
};

interface InitialState {
  modalState: PetModalInterface;
  petData: PetData;
  petVotesData: PetVotesData;
  petCommentsData: PetCommentsData;
  similarPetsData: SimilarPetsData;
  reportReasons: ReportReasons;
}

export interface PetModalInterface {
  opened: boolean;
}

export const initialState: InitialState = {
  modalState: {
    opened: false,
  },
  petData: {
    data: null,
    error: null,
    status: LoadingStatusEnum.LOADING,
  },
  petVotesData: {
    data: [],
    error: null,
    status: LoadingStatusEnum.LOADING,
  },
  petCommentsData: {
    data: [],
    error: null,
    links: {
      first: null,
      last: null,
      next: null,
      prev: null,
    },
    meta: {
      per_page: 15,
      total: 0,
    },
    status: LoadingStatusEnum.LOADING,
    loadingMoreStatus: LoadingStatusEnum.IDLE,
    isCommenting: LoadingStatusEnum.IDLE,
    isDeleting: LoadingStatusEnum.IDLE,
    isMore: true,
    pageToLoad: 1,
  },
  similarPetsData: {
    error: null,
    data: [],
    status: LoadingStatusEnum.LOADING,
  },
  reportReasons: {
    error: null,
    data: [],
    status: LoadingStatusEnum.IDLE,
  },
};
