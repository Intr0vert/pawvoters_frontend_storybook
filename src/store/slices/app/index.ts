import { createSlice } from '@reduxjs/toolkit';
import {
  initialState,
  ModalDataInterface,
  RegisterFinish,
} from './initialState';

const appSlice = createSlice({
  name: 'app',
  initialState,
  reducers: {
    setModalLoginOpened: (
      state,
      { payload }: { payload: ModalDataInterface }
    ) => {
      state.loginModal = payload;
    },
    setRegisterFinishInfo: (
      state,
      { payload }: { payload: RegisterFinish }
    ) => {
      state.registerFinishInfo = payload;
    },
  },
});

const { actions, reducer } = appSlice;
export const { setModalLoginOpened, setRegisterFinishInfo } = actions;
export default reducer;
