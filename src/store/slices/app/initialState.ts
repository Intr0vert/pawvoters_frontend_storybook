export interface AppInitialInterface {
  loginModal: ModalDataInterface;
  registerFinishInfo: RegisterFinish;
}

export interface ModalDataInterface {
  areaTitle?: undefined | string;
  areaDescription?: undefined | string;
  opened: boolean;
}

export interface RegisterFinish {
  userEmail: string | null;
  userToken: string | null;
  active: boolean;
}

export const initialState: AppInitialInterface = {
  loginModal: {
    opened: false,
  },
  registerFinishInfo: {
    userEmail: null,
    userToken: null,
    active: false,
  },
};
