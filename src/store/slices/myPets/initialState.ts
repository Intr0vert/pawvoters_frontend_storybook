import { LoadingStatusEnum } from 'types/enum/LoadingStatus.enum';
import { HomePost } from 'types/interfaces/HomePost.interface';
import { PaginatedLinks } from 'types/interfaces/common/PaginatedLinks.interface';
import { PaginatedMeta } from 'types/interfaces/common/PaginatedMeta.interface';
import { PetBreed } from 'types/interfaces/PetBreed.interface';

type MyPetData = {
  status: LoadingStatusEnum;
  data: HomePost[];
  meta: PaginatedMeta;
  links: PaginatedLinks;
  error: null;
  pageToLoad: number;
  firstLoad: boolean;
};

type PetBreeds = {
  data: PetBreed[];
  error: null;
  status: LoadingStatusEnum;
};

interface InitialState {
  myPetData: MyPetData;
  petBreeds: PetBreeds;
}

export const initialState: InitialState = {
  myPetData: {
    data: [],
    error: null,
    firstLoad: true,
    links: {
      first: null,
      last: null,
      next: null,
      prev: null,
    },
    meta: {
      per_page: 0,
      total: 0,
    },
    pageToLoad: 1,
    status: LoadingStatusEnum.LOADING,
  },
  petBreeds: {
    data: [],
    error: null,
    status: LoadingStatusEnum.LOADING,
  },
};
