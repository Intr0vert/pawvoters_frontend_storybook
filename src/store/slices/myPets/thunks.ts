import { createAsyncThunk } from '@reduxjs/toolkit';
import { MyPetsResponseType } from 'api/types/MyPetsResponse.type';
import { ApiErrorInterface } from 'types/interfaces/common/ApiError.interface';
import MainApiProtected from 'api/MainApiProtected';
import { PetBreedsType } from 'api/types/PetBreeds.type';

export const fetchMyPets = createAsyncThunk<
  MyPetsResponseType,
  { page: number },
  {
    rejectValue: ApiErrorInterface;
  }
>('myPets/loadPets', async ({ page }, { rejectWithValue }) => {
  try {
    return await MainApiProtected.getInstance().getMyPets(page);
  } catch (error) {
    return rejectWithValue(error);
  }
});

export const getBreeds = createAsyncThunk<
  PetBreedsType,
  { petType: string },
  {
    rejectValue: ApiErrorInterface;
  }
>('myPets/getBreeds', async ({ petType }, { rejectWithValue }) => {
  try {
    return await MainApiProtected.getInstance().getBreeds(petType);
  } catch (error) {
    return rejectWithValue(error);
  }
});
