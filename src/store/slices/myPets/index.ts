import { createSlice } from '@reduxjs/toolkit';
import { initialState } from './initialState';
import { fetchMyPets, getBreeds } from './thunks';
import { LoadingStatusEnum } from '../../../types/enum/LoadingStatus.enum';

const userSlice = createSlice({
  name: 'myPets',
  initialState,
  reducers: {
    setIsFirstLoad: (state, { payload }: { payload: boolean }) => {
      state.myPetData.firstLoad = payload;
    },
    resetMyPets: (state) => {
      state.myPetData.data = initialState.myPetData.data;
      state.myPetData.meta = initialState.myPetData.meta;
      state.myPetData.links = initialState.myPetData.links;
      state.myPetData.firstLoad = true;
      state.myPetData.pageToLoad = 1;
      state.myPetData.status = LoadingStatusEnum.LOADING;
    },
    resetBreed: (state) => {
      state.petBreeds.data = initialState.petBreeds.data;
      state.petBreeds.status = initialState.petBreeds.status;
    },
  },
  extraReducers: (builder) => {
    builder.addCase(fetchMyPets.pending, (state) => {
      state.myPetData.status = LoadingStatusEnum.LOADING;
      state.myPetData.error = null;
    });

    builder.addCase(fetchMyPets.rejected, (state, { payload }) => {
      state.myPetData.error = payload?.data;
      state.myPetData.status = LoadingStatusEnum.ERROR;
    });

    builder.addCase(fetchMyPets.fulfilled, (state, { payload }) => {
      if (state.myPetData.firstLoad) {
        state.myPetData.data = payload.data.data;
      } else {
        state.myPetData.data = [...state.myPetData.data, ...payload.data.data];
      }

      state.myPetData.meta = payload.data.meta;
      state.myPetData.links = payload.data.links;
      state.myPetData.pageToLoad = state.myPetData.pageToLoad + 1;
      state.myPetData.status = LoadingStatusEnum.SUCCESS;
    });

    builder.addCase(getBreeds.rejected, (state, { payload }) => {
      state.petBreeds.error = payload?.data;
      state.petBreeds.status = LoadingStatusEnum.ERROR;
    });

    builder.addCase(getBreeds.fulfilled, (state, { payload }) => {
      state.petBreeds.data = payload.data;

      state.petBreeds.status = LoadingStatusEnum.SUCCESS;
    });
  },
});

const { actions, reducer } = userSlice;
export const { setIsFirstLoad, resetMyPets, resetBreed } = actions;
export default reducer;
