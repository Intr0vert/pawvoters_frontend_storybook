import MainApiProtected from 'api/MainApiProtected';
import { createAsyncThunk } from '@reduxjs/toolkit';
import { ApiErrorInterface } from 'types/interfaces/common/ApiError.interface';
import { UserNotificationsType } from 'api/types/UserNotifications.type';
import { UserUnreadNotificationsType } from 'api/types/UserUnreadNotifications.type';
import { RootState } from '../../store';

export const getNotifications = createAsyncThunk<
  UserNotificationsType,
  {},
  {
    rejectValue: ApiErrorInterface;
  }
>('user/getNotifications', async (_, { rejectWithValue }) => {
  try {
    return MainApiProtected.getInstance().getNotifications(1);
  } catch (error) {
    return rejectWithValue(error);
  }
});

export const getUnreadNotifications = createAsyncThunk<
  UserUnreadNotificationsType,
  {},
  {
    rejectValue: ApiErrorInterface;
  }
>('user/getUnreadNotifications', async (_, { rejectWithValue }) => {
  try {
    return MainApiProtected.getInstance().getUnreadNotifications();
  } catch (error) {
    return rejectWithValue(error);
  }
});

export const loadMore = createAsyncThunk<
  UserNotificationsType,
  { page?: number },
  {
    rejectValue: ApiErrorInterface;
  }
>('user/loadMore', async ({ page }, { rejectWithValue, getState }) => {
  const state = getState() as RootState;

  try {
    return MainApiProtected.getInstance().getNotifications(
      page || state.user.notifications.pageToLoad
    );
  } catch (error) {
    return rejectWithValue(error);
  }
});
