import { UserType } from 'types/user/User.type';
import { LoadingStatusEnum } from 'types/enum/LoadingStatus.enum';
import { PaginatedLinks } from 'types/interfaces/common/PaginatedLinks.interface';
import { PaginatedMeta } from 'types/interfaces/common/PaginatedMeta.interface';
import { UserNotification } from 'types/user/UserNotification.type';

export interface UserInitialState {
  notifications: notification;
  token: string | null;
  isAuth: boolean;
  data: null | UserType;
}

type notification = {
  data: UserNotification[];
  links: PaginatedLinks;
  meta: PaginatedMeta;
  status: LoadingStatusEnum;
  pageToLoad: number;
  error: null;
  unread: number;
  firstLoad: boolean;
  opened: boolean;
};

export const initialState: UserInitialState = {
  token: null,
  isAuth: false,
  data: null,
  notifications: {
    data: [],
    error: null,
    links: {
      first: null,
      last: null,
      prev: null,
      next: null,
    },
    meta: {
      per_page: 16,
      total: 0,
    },
    pageToLoad: 1,
    firstLoad: false,
    unread: 0,
    opened: false,
    status: LoadingStatusEnum.IDLE,
  },
};
