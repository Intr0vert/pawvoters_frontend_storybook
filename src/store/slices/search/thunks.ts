import MainApi from 'api/MainApi';
import { createAsyncThunk } from '@reduxjs/toolkit';
import { RootState } from '../../store';
import { ApiErrorInterface } from 'types/interfaces/common/ApiError.interface';
import { SearchPostsType } from 'api/types/SearchPosts.type';

export const getPosts = createAsyncThunk<
  SearchPostsType,
  { query: string },
  {
    rejectValue: ApiErrorInterface;
  }
>('search/getPosts', async ({ query }, { rejectWithValue }) => {
  try {
    return MainApi.getInstance().getSearchQuery(query, 1);
  } catch (error) {
    return rejectWithValue(error);
  }
});

export const loadMore = createAsyncThunk<
  SearchPostsType,
  { query: string; page?: number },
  {
    rejectValue: ApiErrorInterface;
  }
>('search/loadMore', async ({ query, page }, { rejectWithValue, getState }) => {
  try {
    const state = getState() as RootState;

    return MainApi.getInstance().getSearchQuery(
      query,
      page || state.search.pageToLoad
    );
  } catch (error) {
    return rejectWithValue(error);
  }
});

export const getPostsAutoComplete = createAsyncThunk<
  SearchPostsType,
  { query: string; per_page: number },
  {
    rejectValue: ApiErrorInterface;
  }
>(
  'search/getPostsAutoComplete',
  async ({ query, per_page }, { rejectWithValue }) => {
    try {
      return MainApi.getInstance().getSearchQuery(query, 1, per_page);
    } catch (error) {
      return rejectWithValue(error);
    }
  }
);
