import { HomePost } from 'types/interfaces/HomePost.interface';
import { LoadingStatusEnum } from 'types/enum/LoadingStatus.enum';
import { PaginatedMeta } from 'types/interfaces/common/PaginatedMeta.interface';
import { PaginatedLinks } from 'types/interfaces/common/PaginatedLinks.interface';
import { LastVoteCardItemInterface } from 'types/interfaces/LastVotesCardItem.interface';
import { HomeGridOrder } from 'types/home/Order.type';
import { TestimonialItemType } from '../../../types/home/TestimonialItem.type';

export const initialState: IInitialState = {
  pets: {
    status: LoadingStatusEnum.LOADING,
    order: 'popular',
    data: [],
    meta: {
      per_page: 0,
      total: 0,
    },
    links: {
      first: null,
      last: null,
      next: null,
      prev: null,
    },
    error: null,
    pageToLoad: 1,
    firstLoad: true,
  },
  lastVotes: {
    data: [],
    error: null,
    status: LoadingStatusEnum.LOADING,
  },
  testimonials: {
    data: [],
    error: null,
    status: LoadingStatusEnum.LOADING,
  },
};

interface IInitialState {
  pets: HomePets;
  lastVotes: LastVotesCardInterface;
  testimonials: TestimonialsCardInterface;
}

export interface HomePets {
  status: LoadingStatusEnum;
  order: HomeGridOrder;
  data: HomePost[];
  meta: PaginatedMeta;
  links: PaginatedLinks;
  error: any;
  pageToLoad: number;
  firstLoad: boolean;
}

export interface LastVotesCardInterface {
  status: LoadingStatusEnum;
  data: LastVoteCardItemInterface[];
  error: any;
}

export interface TestimonialsCardInterface {
  status: LoadingStatusEnum;
  error: any;
  data: TestimonialItemType[];
}
