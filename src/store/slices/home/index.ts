import { createSlice } from '@reduxjs/toolkit';
import { initialState } from './initialState';
import { LoadingStatusEnum } from 'types/enum/LoadingStatus.enum';
import {
  fetchHomePagePets,
  fetchLastVotes,
  fetchTestimonialItems,
} from './thunks';
import { homePetsPrepare } from 'lib/utils/data-prepare';

const homeSlice = createSlice({
  name: 'home',
  initialState,
  reducers: {
    resetHomeState: (state) => {
      state.pets = {
        ...initialState.pets,
        order: state.pets.order,
      };
      state.lastVotes = initialState.lastVotes;
    },
    setOrder: (state, { payload }) => {
      state.pets.order = payload;
    },
  },
  extraReducers: (builder) => {
    builder.addCase(fetchHomePagePets.fulfilled, (state, { payload }) => {
      if (state.pets.firstLoad) {
        state.pets.data = homePetsPrepare(payload.data.data);
        state.pets.firstLoad = false;
      } else {
        state.pets.data = [
          ...state.pets.data,
          ...homePetsPrepare(payload.data.data),
        ];
      }
      state.pets.meta = payload.data.meta;
      state.pets.links = payload.data.links;
      state.pets.pageToLoad = state.pets.pageToLoad + 1;
      state.pets.status = LoadingStatusEnum.SUCCESS;
    });

    builder.addCase(fetchHomePagePets.pending, (state) => {
      state.pets.status = LoadingStatusEnum.LOADING;
      state.pets.error = null;
    });

    builder.addCase(fetchHomePagePets.rejected, (state, { payload }) => {
      state.pets.status = LoadingStatusEnum.ERROR;
      state.pets.error = payload?.data;
    });

    builder.addCase(fetchLastVotes.fulfilled, (state, { payload }) => {
      state.lastVotes.status = LoadingStatusEnum.SUCCESS;
      state.lastVotes.data = payload.data;
    });

    builder.addCase(fetchLastVotes.pending, (state) => {
      state.lastVotes.status = LoadingStatusEnum.LOADING;
      state.lastVotes.error = null;
    });

    builder.addCase(fetchLastVotes.rejected, (state, { payload }) => {
      state.pets.status = LoadingStatusEnum.ERROR;
      state.pets.error = payload?.data;
    });

    builder.addCase(fetchTestimonialItems.pending, (state) => {
      state.testimonials.status = LoadingStatusEnum.LOADING;
      state.testimonials.error = null;
    });

    builder.addCase(fetchTestimonialItems.rejected, (state, { payload }) => {
      state.testimonials.status = LoadingStatusEnum.ERROR;
      state.testimonials.error = payload?.data;
    });

    builder.addCase(fetchTestimonialItems.fulfilled, (state, { payload }) => {
      state.testimonials.data = payload.data.data;
      state.testimonials.status = LoadingStatusEnum.SUCCESS;
    });
  },
});

const { reducer, actions } = homeSlice;
export const { resetHomeState, setOrder } = actions;
export default reducer;
