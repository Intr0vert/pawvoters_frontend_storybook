import { createAsyncThunk } from '@reduxjs/toolkit';
import MainApi from 'api/MainApi';
import { LeaderboardType } from 'api/types/LeaderboardPosts.type';
import { ApiErrorInterface } from 'types/interfaces/common/ApiError.interface';

export const getLeaderboard = createAsyncThunk<
  LeaderboardType,
  {},
  {
    rejectValue: ApiErrorInterface;
  }
>('myPets/loadPets', async (_, { rejectWithValue }) => {
  try {
    return await MainApi.getInstance().getLeaderboard();
  } catch (error) {
    return rejectWithValue(error);
  }
});
