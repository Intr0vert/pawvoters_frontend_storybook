import { createSlice } from '@reduxjs/toolkit';
import { initialState } from './initialState';
import { LoadingStatusEnum } from 'types/enum/LoadingStatus.enum';
import { getLeaderboard } from './thunks';

const leaderboardSlice = createSlice({
  name: 'leaderboard',
  initialState,
  reducers: {},
  extraReducers: (builder) => {
    builder.addCase(getLeaderboard.fulfilled, (state, { payload }) => {
      state.data = payload.data.data;
      state.links = payload.data.links;
      state.meta = payload.data.meta;
    });
  },
});

const { reducer } = leaderboardSlice;
export default reducer;
