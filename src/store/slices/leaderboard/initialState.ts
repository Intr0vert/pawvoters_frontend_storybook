import { PaginatedMeta } from './../../../types/interfaces/common/PaginatedMeta.interface';
import { PaginatedLinks } from './../../../types/interfaces/common/PaginatedLinks.interface';
import { LoadingStatusEnum } from 'types/enum/LoadingStatus.enum';
import { LeaderboardPost } from 'types/interfaces/LeaderboardPost.interface';

export type InitialState = {
  data: LeaderboardPost[];
  error: null;
  meta: PaginatedMeta;
  links: PaginatedLinks;
  status: LoadingStatusEnum;
};

export const initialState = {
  data: [],
  error: null,
  links: {
    first: null,
    last: null,
    next: null,
    prev: null,
  },
  meta: {
    per_page: 0,
    total: 0,
  },
  status: LoadingStatusEnum.LOADING,
} as InitialState;
