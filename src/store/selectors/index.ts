import { RootState } from '../store';

export const homePetsSelector = ({ home }: RootState) => home.pets;
export const lastVotesSelector = ({ home }: RootState) => home.lastVotes;
export const homeTestimonialsSelector = ({ home }: RootState) =>
  home.testimonials;

export const loginModalSelector = ({ app }: RootState) => app.loginModal;
export const finishRegistrationSelector = ({ app }: RootState) =>
  app.registerFinishInfo;

export const petModalData = ({ pet }: RootState) => pet.modalState;
export const petDataSelector = ({ pet }: RootState) => pet.petData.data;
export const petStatusSelector = ({ pet }: RootState) => pet.petData.status;
export const petVotesSelector = ({ pet }: RootState) => pet.petVotesData;
export const petCommentsSelector = ({ pet }: RootState) => pet.petCommentsData;
export const petSimilarSelector = ({ pet }: RootState) => pet.similarPetsData;
export const petReportReasonsSelector = ({ pet }: RootState) =>
  pet.reportReasons;

export const searchDataSelector = ({ search }: RootState) => search.data;
export const searchSelector = ({ search }: RootState) => search;
export const searchAutoCompeleteDataSelector = ({ search }: RootState) =>
  search.autoCompleteData;
