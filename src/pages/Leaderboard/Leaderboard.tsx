import React from 'react';
import s from './Leaderboard.module.scss';
import LeaderboardMain from 'components/LeaderboardPage/LeaderboardMain';

const Leaderboard = () => {
  return (
    <div className={s.root}>
      <div className={s.container}>
        <LeaderboardMain />
      </div>
    </div>
  );
};

export default Leaderboard;
