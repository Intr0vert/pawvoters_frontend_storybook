import React from 'react';
import s from './Contest.module.scss';
import ContestMain from 'components/ContestPage/ContestMain';

const Contest = () => {
  return (
    <div>
      <ContestMain />
    </div>
  );
};

export default Contest;
