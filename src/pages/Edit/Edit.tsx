import React, { useState } from 'react';
import s from './Edit.module.scss';
import Breadcrumbs from 'components/ui/Breadcrumbs';
import { Typography } from '@material-ui/core';
import EditPageMain from 'components/EditPage/EditPageMain';
import { RemoveIcon } from 'components/icons';
import { useSelector } from 'store/store';
import { finishRegistrationSelector } from 'store/selectors';
import MainApiProtected from '../../api/MainApiProtected';
import { createErrorToast, createSuccessToast } from '../../lib/utils/toasts';
import { useHistory } from 'react-router-dom';
import { petDataSelector } from 'store/selectors/index';

const Edit = () => {
  const [submit, setSubmit] = useState<boolean>(false);
  const finishRegistrationData = useSelector(finishRegistrationSelector);
  const myPetData = useSelector(petDataSelector);
  const history = useHistory();

  const handleClick = async () => {
    // eslint-disable-next-line no-restricted-globals
    const answer = confirm('Are you sure?');

    if (answer) {
      setSubmit(true);
      const result = await MainApiProtected.getInstance().deleteMyPet(
        myPetData?.id
      );

      if (result.status !== 200) {
        createErrorToast({
          message: 'Something went wrong, please try again',
        });
        setSubmit(false);

        return;
      }

      createSuccessToast({ message: 'Your pet was deleted' });
      setSubmit(false);
      history.push('/pets');
    }
  };

  return (
    <div className={s.root}>
      <div className={s.container}>
        <Breadcrumbs
          className={s.breadcrumbs}
          items={[
            { id: 1, href: `/`, label: 'Home' },
            { id: 2, href: '/edit/:slug', label: 'Edite Pet' },
          ]}
        />
        <Typography variant="h1" className={s.title}>
          Edite Pet
          {!finishRegistrationData.active && (
            <button
              type="button"
              className={s.deleteAccountP} // p => PC
              onClick={handleClick}
              disabled={submit}
            >
              <RemoveIcon />
              Delete pet
            </button>
          )}
        </Typography>
        <EditPageMain />
        {!finishRegistrationData.active && (
          <button
            type="button"
            className={s.deleteAccountM} // m => MOBILE
            onClick={handleClick}
            disabled={submit}
          >
            <RemoveIcon />
            Delete pet
          </button>
        )}
      </div>
    </div>
  );
};

export default Edit;
