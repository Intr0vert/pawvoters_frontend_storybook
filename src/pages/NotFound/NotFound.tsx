import Container from '@material-ui/core/Container';
import React from 'react';
import s from './NotFound.module.scss';
import cn from 'classnames';
import { Typography } from '@material-ui/core';
import { NotFoundIcon } from '../../components/icons';
import Button from '../../components/ui/Button';
import { Link } from 'react-router-dom';

const NotFound = () => {
  return (
    <Container className={s.container}>
      <div className={cn(s.wrap)}>
        <div className={cn(s.actions)}>
          <Typography variant="h1">404</Typography>
          <Typography variant="h4">Page does not exist</Typography>
          <Link to="/">
            <Button variant="contained" color="primary">
              Back to home
            </Button>
          </Link>
        </div>
        <div className={cn(s.image)}>
          <NotFoundIcon />
        </div>
      </div>
    </Container>
  );
};

export default NotFound;
