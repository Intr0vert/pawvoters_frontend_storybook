import React from 'react';
import s from './Add.module.scss';
import { Typography } from '@material-ui/core';
import Breadcrumbs from 'components/ui/Breadcrumbs';
import AddPageMain from 'components/AddPage/AddPageMain';

const Add = () => (
  <div className={s.root}>
    <div className={s.container}>
      <Breadcrumbs
        className={s.breadcrumbs}
        items={[
          { id: 1, href: `/`, label: 'Home' },
          { id: 0, href: '/add', label: 'Add pet' },
        ]}
      />
      <Typography variant="h1" className={s.title}>
        Add Pet
      </Typography>
      <AddPageMain />
    </div>
  </div>
);

export default Add;
