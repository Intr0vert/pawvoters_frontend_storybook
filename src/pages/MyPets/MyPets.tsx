import React, { useEffect, useRef, useMemo } from 'react';
import Container from '@material-ui/core/Container';
import s from './MyPets.module.scss';
import cn from 'classnames';
import Breadcrumbs from 'components/ui/Breadcrumbs';
import { CircularProgress, Typography } from '@material-ui/core';
import { useAppDispatch, useSelector } from '../../store/store';
import { useThrottledEvent } from '../../lib/hooks/useThrottledEvent';
import { fetchMyPets } from '../../store/slices/myPets/thunks';
import { createErrorToast } from 'lib/utils/toasts';
import { myPetsSelector } from '../../store/selectors/myPets';
import { LoadingStatusEnum } from '../../types/enum/LoadingStatus.enum';
import StackGrid from 'react-stack-grid';
import MyPetsAdd from '../../components/cards/MyPetsAdd/MyPetsAdd';
import Pet from '../../components/cards/Pet';
import NoPets from '../../components/icons/NoPets';
import { useInView } from 'react-intersection-observer';
import { resetMyPets, setIsFirstLoad } from 'store/slices/myPets';
import EditIcon from '../../components/icons/Edit';
import { generatePetSlug } from 'lib/utils/url-utils';
import { fetchPostData } from 'store/slices/pet/thunks';
import useResizeObserver from 'use-resize-observer/polyfilled';
import { useHistory } from 'react-router';

const MyPets = () => {
  const myPets = useSelector(myPetsSelector);
  const dispatch = useAppDispatch();
  const gridRef = useRef();
  const history = useHistory();

  useThrottledEvent({
    callback: () => {
      //@ts-ignore
      gridRef.current?.updateLayout();
    },
    event: 'resize',
    throttleTime: 1000,
  });
  const { ref, inView } = useInView({
    threshold: 0,
  });
  const { ref: refWidth, width = 1 } = useResizeObserver<HTMLDivElement>();

  const gridWith = useMemo(() => {
    return width < 640
      ? '100%'
      : width > 640 && width < 980
      ? '50%'
      : width > 980
      ? '33.33%'
      : 284;
  }, [width]);

  useEffect(() => {
    dispatch(fetchMyPets({ page: 1 })).then((result) => {
      if (result.payload?.status === 200) {
        setTimeout(() => {
          dispatch(setIsFirstLoad(false));
        }, 200);
      }
    });

    return () => {
      dispatch(resetMyPets());
    };
  }, [dispatch]);

  useEffect(() => {
    if (
      myPets.myPetData.status !== LoadingStatusEnum.LOADING &&
      myPets.myPetData.status !== LoadingStatusEnum.ERROR &&
      inView
    ) {
      dispatch(fetchMyPets({ page: myPets.myPetData.pageToLoad }));
    }
  }, [inView]);

  const handleClick = async (petName: string, postId: number) => {
    const result = await dispatch(fetchPostData({ postId }));

    if (result.payload?.status === 200) {
      return history.push(`/edit/${generatePetSlug(petName, postId)}`);
    }

    createErrorToast({ message: 'Something went wrong, please try again' });
  };

  return (
    <div className={cn(s.root)} ref={refWidth}>
      <Container maxWidth="md" className={s.container}>
        <Breadcrumbs
          className={s.breadcrumbs}
          items={[
            { id: 0, href: '/contest', label: 'Contest' },
            { id: 1, href: '/pets', label: 'My Pets' },
          ]}
        />
        <Typography variant="h1">My Pets</Typography>
        {myPets.myPetData.status === LoadingStatusEnum.LOADING &&
        myPets.myPetData.firstLoad ? (
          <div className={s.preloader}>
            <CircularProgress size={48} />
          </div>
        ) : (
          <>
            {myPets.myPetData.data.length > 0 ? (
              <>
                <div className={s.gridRoot}>
                  <StackGrid
                    //@ts-ignore
                    gridRef={(grid) => (gridRef.current = grid)}
                    monitorImagesLoaded
                    columnWidth={gridWith}
                    gutterWidth={30}
                    gutterHeight={30}
                    appearDelay={0}
                    duration={0}
                  >
                    <MyPetsAdd />
                    {myPets.myPetData.data.map((item: any) => (
                      <div key={item.id}>
                        <div
                          className={s.editPet}
                          onClick={() => handleClick(item.petName, item.id)}
                        >
                          <EditIcon />
                        </div>
                        <Pet {...item} />
                      </div>
                    ))}
                  </StackGrid>
                </div>
                {Boolean(myPets.myPetData.links.next) &&
                  !myPets.myPetData.firstLoad &&
                  myPets.myPetData.status === LoadingStatusEnum.SUCCESS && (
                    <div className={s.observerMarker} ref={ref} />
                  )}
                {Boolean(myPets.myPetData.links.next) &&
                  myPets.myPetData.status === LoadingStatusEnum.LOADING &&
                  !myPets.myPetData.firstLoad && (
                    <div className={s.moreLoaderWrapper}>
                      <CircularProgress size={48} />
                    </div>
                  )}
              </>
            ) : (
              <>
                <div className={cn(s.wrap)}>
                  <MyPetsAdd />
                  <div className={cn(s.noPetsCard)}>
                    <Typography variant="h4">
                      There are no pets here yet
                    </Typography>
                    <NoPets />
                  </div>
                </div>
              </>
            )}
          </>
        )}
      </Container>
    </div>
  );
};

export default MyPets;
