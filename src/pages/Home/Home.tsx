import Container from '@material-ui/core/Container';
import React, { FC, useEffect, useMemo, useRef } from 'react';
import s from './Home.module.scss';
import cn from 'classnames';
import Banner from '../../components/homePage/Banner';
import SeoSlider from 'components/common/SeoSlider';
import LoadMore from '../../components/homePage/LoadMore';
import { useAppDispatch, useSelector } from 'store/store';
import { homePetsSelector } from 'store/selectors';
import { fetchHomePagePets } from 'store/slices/home/thunks';
import StackGrid from 'react-stack-grid';
import HomeGridFactory from 'components/factories/HomeGridFactory';
import { useThrottledEvent } from 'lib/hooks/useThrottledEvent';
import HomeSwitcher from '../../components/homePage/HomeSwitcher';
import HomeLoadingState from '../../components/homePage/HomeLoadingState';
import { resetHomeState } from 'store/slices/home';
import useResizeObserver from 'use-resize-observer/polyfilled';
import { useLocation } from 'react-router-dom';
import { parse } from 'query-string';
import { setOrder } from 'store/slices/home';

const Home: FC = () => {
  const dispatch = useAppDispatch();
  const pets = useSelector(homePetsSelector);
  const gridRef = useRef();
  useThrottledEvent({
    callback: () => {
      //@ts-ignore
      gridRef.current?.updateLayout();
    },
    event: 'resize',
    throttleTime: 1000,
  });
  const { ref, width = 1 } = useResizeObserver<HTMLDivElement>();
  const location = useLocation();

  useEffect(() => {
    const parsedQuery = parse(location.search);
    const order = parsedQuery.order as 'popular' | 'recent' | undefined;

    if (order === 'recent') {
      dispatch(setOrder(order));
    } else {
      dispatch(setOrder('popular'));
    }

    dispatch(fetchHomePagePets({ page: 1 }));

    return () => {
      dispatch(resetHomeState());
    };
  }, [dispatch, location.search]);

  const gridWith = useMemo(() => {
    return width < 480
      ? '100%'
      : width < 900 && width >= 480
      ? '50%'
      : width > 900 && width < 1200
      ? '33.33%'
      : width >= 1200 && width <= 1560
      ? '25%'
      : width > 1560
      ? '20%'
      : 284;
  }, [width]);

  return (
    <div className={cn(s.root)} ref={ref}>
      <Container className={s.container}>
        <Banner />
        <HomeSwitcher />
      </Container>
      <HomeLoadingState />
      <Container className={cn(s.container, s.gridContainer)}>
        <StackGrid
          //@ts-ignore
          gridRef={(grid) => (gridRef.current = grid)}
          monitorImagesLoaded
          columnWidth={gridWith}
          gutterWidth={30}
          gutterHeight={30}
          appearDelay={0}
          duration={0}
        >
          {pets.data.map((item, index) => (
            <HomeGridFactory
              component={item}
              key={item.id + (index * 5000).toString()}
            />
          ))}
        </StackGrid>
      </Container>
      <LoadMore />
      <SeoSlider />
    </div>
  );
};

export default Home;
