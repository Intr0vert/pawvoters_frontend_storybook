import Container from '@material-ui/core/Container';
import React from 'react';
import s from './About.module.scss';
import cn from 'classnames';
import SeoSlider from '../../components/common/SeoSlider';
import StaticBanner from '../../components/common/StaticBanner';
import { Typography } from '@material-ui/core';



const Privacy = () => {
  return (
    <div className={cn(s.root)}>
      <Container className={s.container}>
        <StaticBanner title="About us" />
        <div className={cn(s.textBlocRoot)}>
          <div className={cn(s.textBlocMain)}>
            <Typography variant="body2">Please read the following statements carefully, as they address the rules of engagement between us.
            Your privacy is critically important to us. We want to assure you we respect your data protection rights in compliance with the latest EU General Data Protection Regulation (GDPR) requirements as of 25th May 2018.
            is Website is owned and operated by Online Growth Lab Ltd. The following terms and conditions govern your use of this site and all other Online Growth Lab Limited Sites and products, some of which (but not all) are listed herein.

            the purpose of this agreement and for ease of use, any reference to “the Company” shall be a reference to Online Growth Lab Ltd and any reference to “the Website” shall be a reference to any and all websites owned by the Company, their respective products, and their employees and agents.

  B           accessing this Website and any of the Company’s properties, you are acknowledging and accepting the conditions of this Privacy Policy for all of the Company’s properties.</Typography>
          </div>
        </div>
      </Container>
      <SeoSlider />
    </div>
  );
};

export default Privacy;
