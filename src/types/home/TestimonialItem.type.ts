interface User {
  id: number;
  nickname: string;
  image: string;
  backgroundColor: string;
}

interface Post {
  id: number;
  petName: string;
  petType: string;
  image: string;
  backgroundColor: string;
}

interface Verification {
  facebook: boolean;
  email: boolean;
  id: boolean;
  ownership: boolean;
}

export type TestimonialItemType = {
  user: User;
  post: Post;
  date: Date;
  verification: Verification;
  prize: number;
  text: string;
};
