export type PetType = {
  id: number;
  petName: string;
  breed: {
    id: number;
    name: string;
    petType: 'cat' | 'dog';
  };
  petType: 'cat' | 'dog';
  bio: string;
  isNew: boolean;
  image: string;
  backgroundColor: string;
  countVotes: number;
  activeContests: number[];
  lastContests: number[];
  instaLink: null | string;
  fbLink: null | string;
  user: {
    id: number;
    nickname: string;
    image: string;
    location: string;
    city: string;
  };
};
