export type ReportItemType = {
  code: string;
  title: string;
};
