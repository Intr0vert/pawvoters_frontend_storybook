import { ReplyItemType } from './ReplyItem.type';

export type CommentItemType = {
  id: number;
  text: string;
  created: string;
  parentId: number;
  isAlreadyReportedByMe: boolean;
  user: {
    id: number;
    nickname: string;
    image: string;
  };
  totalReplies: number;
  replies: ReplyItemType[];
};
