export type ReplyItemType = {
  id: number;
  text: string;
  created: string;
  parentId: number;
  isAlreadyReportedByMe: boolean;
  user: {
    id: number;
    nickname: string;
    image: string;
  };
};
