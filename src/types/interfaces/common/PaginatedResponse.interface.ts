import { PaginatedMeta } from './PaginatedMeta.interface';
import { PaginatedLinks } from './PaginatedLinks.interface';

export interface PaginatedResponseInterface {
  meta: PaginatedMeta;
  links: PaginatedLinks;
}
