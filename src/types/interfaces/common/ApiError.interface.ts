export interface ApiErrorInterface {
  data: any;
  status: number;
}