export interface PaginatedMeta {
  total: number;
  per_page: number;
}
