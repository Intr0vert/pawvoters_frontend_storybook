export interface CommonResponseInterface<T = any> {
  data: T;
}
