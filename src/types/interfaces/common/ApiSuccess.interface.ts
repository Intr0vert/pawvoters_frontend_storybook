import { PaginatedResponseInterface } from './PaginatedResponse.interface';

export interface ApiSuccessInterface<T> {
  data: T;
  status: number;
}

export interface ApiSuccessCommonInterface<T> {
  data: {
    data: T;
  };
  status: number;
}

export interface ApiSuccessPaginatedInterface<T> {
  data: {
    data: T;
  } & PaginatedResponseInterface;
  status: number;
}
