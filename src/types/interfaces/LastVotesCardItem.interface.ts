export interface LastVoteCardItemInterface {
  id: number;
  created: string;
  userId: number;
  userImage: string;
  userNickname: string;
  postId: number;
  postPetName: string;
}