export interface PetBreed {
  id: number;
  name: string;
  petType: string;
}
