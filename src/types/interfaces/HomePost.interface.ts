import { HomeCardsEnum } from '../enum/HomeCards.enum';

export interface HomePost {
  id: number | string;
  petName: string;
  isNew: boolean;
  image: string;
  backgroundColor: string;
  countVotes: number;
  activeContests: number[];
  userId: number;
  userNickname: string;
  type?: HomeCardsEnum;
  originalImageWidth: number;
  originalImageHeight: number;
}
