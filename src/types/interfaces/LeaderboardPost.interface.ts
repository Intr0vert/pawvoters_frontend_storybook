export interface LeaderboardPost {
  contest: {
    id: number;
    title: string;
    startedAt: string | number;
    finishedAt: string | number;
  };
  posts: {
    id: number;
    petName: string;
    image: string;
    countVotes: number;
    place: number;
    prize: string | number;
  };
}
