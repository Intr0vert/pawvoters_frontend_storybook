export enum LoadingStatusEnum {
  IDLE = 'idle',
  ERROR = 'error',
  LOADING = 'loading',
  SUCCESS = 'success'
}