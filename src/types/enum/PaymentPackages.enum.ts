export enum PaymentPackagesEnum {
  FREE = 'free',
  REGULAR = 'regular',
  SPECIAL = 'special',
}
