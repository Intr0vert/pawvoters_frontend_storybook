export interface Country {
  id: number;
  code: string;
  title: string;
}

export interface EmailNotificationSettings {
  voteMyPost: boolean;
  commentPostWithMyVote: boolean;
  commentMyPost: boolean;
  replyMyComment: boolean;
}

export interface UserType {
  apiToken: string;
  id: number;
  nickname: string;
  isAnyPets: number;
  image: string;
  backgroundColor: string;
  location: string;
  city: string;
  country: Country;
  email: string;
  facebookId: string;
  googleId: string;
  emailNotificationSettings: EmailNotificationSettings;
  unsubscribe: number;
  timeToFreeVote: number;
  hasUnreadNotifications: boolean;
  mirror: string;
}
