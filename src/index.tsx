import React from 'react';
import ReactDOM from 'react-dom';
import './styles/main.scss';
import 'react-toastify/dist/ReactToastify.css';
import App from './App';
import reportWebVitals from './reportWebVitals';
import { BrowserRouter } from 'react-router-dom';
import { MuiThemeProvider } from '@material-ui/core';
import { theme } from './theme/theme';
import { ToastContainer } from 'react-toastify';
import { Close } from './components/icons';
import store from './store/store';
import { Provider } from 'react-redux';

ReactDOM.render(
  <BrowserRouter>
    <MuiThemeProvider theme={theme}>
      <ToastContainer
        limit={3}
        position={'bottom-left'}
        autoClose={5000}
        closeButton={
          <i style={{ color: '#fff' }}>
            <Close />
          </i>
        }
      />
      <Provider store={store}>
        <App />
      </Provider>
    </MuiThemeProvider>
  </BrowserRouter>,
  document.getElementById('root')
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
