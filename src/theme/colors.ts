const colors = {
  primary: '#eb864b',
  primaryDark: '#d26d32',
  secondary: '#e9b32a',
  secondaryHover: '#dfa717',
  black: '#31344e',
  bg1: '#ffffff',
  redERror: '#f03738',
  fiolet: '#5c6191',
  gray: '#f4f9fe',
  gray2: '#a4afbe',
  disabled: '#f4f5f7',
};

export default colors;
