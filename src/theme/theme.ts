import { createMuiTheme } from '@material-ui/core';
import colors from './colors';

const theme = createMuiTheme({
  palette: {
    primary: {
      main: colors.primary,
      dark: colors.primaryDark,
    },
    secondary: {
      main: colors.secondary,
    },
    text: {
      primary: colors.black,
    },
    background: {
      default: colors.bg1,
    },
    error: {
      main: colors.redERror,
    },
  },
  typography: {
    fontFamily: ['SF Pro Display', 'sans-serif'].join(','),
    h1: {
      fontSize: 64,
      lineHeight: '76px',
      fontWeight: 600,
    },
    h2: {
      fontSize: 48,
      lineHeight: '57px',
      fontWeight: 600,
    },
    h3: {
      fontSize: 36,
      lineHeight: '43px',
      fontWeight: 600,
    },
    h4: {
      fontSize: 24,
      lineHeight: '29px',
      fontWeight: 600,
    },
    body1: {
      fontSize: 17,
      lineHeight: '27px',
    },
    body2: {
      fontSize: 15,
      lineHeight: '22px',
    },
    caption: {
      fontSize: 13,
      lineHeight: '16px',
    },
    overline: {
      fontSize: 11,
      lineHeight: '13px',
      textTransform: 'initial',
    },
    button: {
      fontSize: 14,
      lineHeight: '17px',
      fontWeight: 600,
    },
  },
  overrides: {
    MuiButton: {
      containedSecondary: {
        backgroundColor: colors.secondary,
        '&:hover': {
          backgroundColor: `${colors.secondaryHover} !important`,
        },
      },
    },
    MuiIconButton: {
      root: {
        padding: 0,
        width: 45,
        height: 45,
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
      },
    },
    MuiLink: {
      root: {
        color: colors.fiolet,
        fontWeight: 600,
        '&:hover': {
          color: colors.primary,
          textDecoration: 'none',
        },
        '&.withIcon': {
          display: 'flex',
          alignItems: 'center',
          width: 'fit-content',
          '& svg': {
            marginRight: 10,
          },
        },
      },
      underlineHover: {
        '&:hover': {
          color: colors.primary,
          textDecoration: 'none',
        },
      },
    },
    MuiFilledInput: {
      root: {
        border: `1px solid ${colors.gray2}`,
        borderRadius: 5,
        overflow: 'hidden',
        backgroundColor: colors.bg1,
        '&:hover': {
          backgroundColor: colors.bg1,
        },
        '&:after': {
          display: 'none',
        },
        '&:before': {
          display: 'none',
        },
        '&$focused': {
          backgroundColor: colors.bg1,
          borderColor: colors.primary,
        },
        '&$error': {
          borderColor: colors.redERror,
        },
        '&$disabled': {
          background: colors.disabled,
          borderColor: colors.gray2,
        },
        '&.comment': {
          borderColor: colors.primary,
          borderRadius: '5px 20px 20px 20px',
        },
      },
      input: {
        boxSizing: 'border-box',
        fontSize: '13px',
        padding: '28px 12px 15px',
        color: colors.black,
      },
      inputAdornedEnd: {
        paddingRight: 14,
      },
      multiline: {
        paddingTop: 22,
      },
    },
    MuiFormHelperText: {
      contained: {
        marginLeft: 0,
        position: 'absolute',
        top: '97%',
        fontSize: '13px',
      },
    },
    MuiSelect: {
      root: {
        padding: '12px 12px 11px',
      },
      select: {
        '&:focus': {
          backgroundColor: colors.bg1,
        },
      },
    },
    MuiCheckbox: {
      root: {
        color: colors.primary,
      },
    },
    MuiRadio: {
      root: {
        color: colors.primary,
      },
    },
    MuiFormControlLabel: {
      root: {
        width: 'fit-content',
        '&$disabled': {
          color: colors.gray2,
        },
      },
      label: {
        fontSize: 13,
        fontWeight: 500,
      },
    },
    MuiBreadcrumbs: {
      li: {
        fontSize: 14,
        fontWeight: 600,
        '& p': {
          color: colors.gray2,
          fontSize: 14,
          fontWeight: 600,
        },
      },
      separator: {
        marginLeft: 18,
        marginRight: 18,
      },
    },
    MuiSlider: {
      root: {
        '&$focusVisible': {
          boxShadow: '0px 0px 0px 5px rgba(235, 134, 75, 0.2)',
        },
      },
      rail: {
        height: 8,
        borderRadius: 4,
        backgroundColor: colors.gray2,
        opacity: 1,
      },
      track: {
        height: 8,
        borderRadius: 4,
      },
      thumb: {
        width: 24,
        height: 24,
        marginTop: -9,
        '&:hover': {
          boxShadow: '0px 0px 0px 5px rgba(235, 134, 75, 0.2)',
        },
        '&.Mui-focusVisible': {
          boxShadow: '0px 0px 0px 5px rgba(235, 134, 75, 0.2)',
        },
        '&.MuiSlider-active': {
          boxShadow: '0px 0px 0px 10px rgba(235, 134, 75, 0.2)',
        },
      },
    },
    MuiContainer: {
      root: {
        paddingLeft: '15px!important',
        paddingRight: '15px!important',
        '@media(min-width: 768px)': {
          paddingLeft: '30px!important',
          paddingRight: '30px!important',
        },
      },
      maxWidthLg: {
        '@media (min-width: 1280px)': {
          maxWidth: 1600,
        },
      },
      maxWidthMd: {
        '@media (min-width: 960px)': {
          maxWidth: 972,
        },
      },
    },
  },
});

export { theme };
