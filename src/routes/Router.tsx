import React from 'react';
import { Switch, Route } from 'react-router-dom';
import Home from '../pages/Home';
import About from '../pages/About';
import Privacy from '../pages/Privacy';
import Terms from '../pages/Terms';
import NotFound from '../pages/NotFound';
import Search from '../pages/Search';
import Pet from '../pages/Pet';
import MyPets from '../pages/MyPets';
import Add from '../pages/Add';
import Edit from '../pages/Edit';
import Me from '../pages/Me';
import Contest from '../pages/Contest';
import Support from '../pages/Support';
import PrivateRoute from '../components/common/PrivateRoute';
import LoginConfirmByHash from '../pages/LoginConfirmByHash';
import Leaderboard from '../pages/Leaderboard';

const Router = () => {
  return (
    <Switch>
      <Route path="/" exact>
        <Home />
      </Route>
      <Route path="/terms">
        <Terms />
      </Route>
      <Route path="/privacy">
        <Privacy />
      </Route>
      <Route path="/about">
        <About />
      </Route>
      <Route path="/pets">
        <MyPets />
      </Route>
      <Route path="/search">
        <Search />
      </Route>
      <Route path="/post/:slug">
        <Pet />
      </Route>
      <Route path="/support">
        <Support />
      </Route>
      <Route path="/leaderboard">
        <Leaderboard />
      </Route>
      <Route path="/login/confirm/:hash/auth" exact>
        <LoginConfirmByHash />
      </Route>
      <Route path="/contest">
        <Contest />
      </Route>
      <PrivateRoute path="/add" component={<Add />} />
      <PrivateRoute path="/me" component={<Me />} />
      <PrivateRoute path="/edit/:slug" component={<Edit />} />
      <Route path="*">
        <NotFound />
      </Route>
    </Switch>
  );
};

export { Router };
