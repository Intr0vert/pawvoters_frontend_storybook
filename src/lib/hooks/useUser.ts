import { useSelector } from 'store/store';
import {
  getUserDataSelector,
  getUserIsAuthSelector,
  getUserTokenSelector,
} from 'store/selectors/user';

export const useUser = () => {
  const isAuth = useSelector(getUserIsAuthSelector);
  const userData = useSelector(getUserDataSelector);
  const userToken = useSelector(getUserTokenSelector);

  return {
    isAuth,
    userData,
    userToken,
  };
};
