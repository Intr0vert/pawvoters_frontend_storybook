import throttle from 'lodash.throttle';
import { useEffect } from 'react';

interface Props {
  callback: () => any;
  event?: 'scroll' | 'resize';
  throttleTime?: number;
}

const useThrottledEvent = ({
  callback,
  event = 'scroll',
  throttleTime = 200,
}: Props): void => {
  useEffect(() => {
    const listener = throttle(callback, throttleTime);
    window.addEventListener(event, listener);

    return () => {
      window.removeEventListener(event, listener);
    };
  }, []);
};

export { useThrottledEvent };
