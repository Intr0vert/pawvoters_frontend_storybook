import MainApiProtected from 'api/MainApiProtected';
import { getUnreadNotifications } from 'store/slices/user/thunks';
import { UserNotification } from 'types/user/UserNotification.type';

export const readNotifications = (data: UserNotification[]) => async (
  dispatch: any
) => {
  const idList: number[] = [];

  data.forEach((item) => {
    if (!item.isRead) idList.push(item.id);
  });

  if (idList.length) {
    await MainApiProtected.getInstance().postNotifications({
      notifications: idList.join(','),
    });

    await dispatch(getUnreadNotifications({}));
  }
};
