export const createFormData = (values: any): FormData => {
  const form_data = new FormData();

  for (let key in values) {
    if (values[key]) form_data.append(key, values[key]);
  }

  return form_data;
};
