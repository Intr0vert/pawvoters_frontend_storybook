import React from 'react';
import { toast } from 'react-toastify';
import { ErrorToast, SuccessToast } from '../../components/ui/Toasts';

interface ToastProps {
  message: string;
}

export const createErrorToast = ({ message }: ToastProps) => {
  toast.error(<ErrorToast description={message} />, {
    className: 'toast toast--error',
  });
};

export const createSuccessToast = ({ message }: ToastProps) => {
  toast.success(<SuccessToast description={message} />, {
    className: 'toast toast--success',
  });
};
