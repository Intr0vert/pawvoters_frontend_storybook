import { uppercaseFirstLetter } from './string';

export const generatePetSlug = (petName: string, petId: number | string) => {
  return `${petName.toLowerCase()}_${petId}`;
};

export const parsePetSlug = (url: string): string[] => {
  try {
    const [petName, petId] = url.split('_');
    return [uppercaseFirstLetter(petName), petId];
  } catch (e) {
    throw new Error(`Wrong url was given - ${url}`);
  }
};
