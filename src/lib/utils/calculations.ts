type Options = {
  dimension?: '%' | 'px';
  maxHeightValue?: number;
  minHeightValue?: number;
  staticHeight?: boolean;
};

export const calculatePetCardHeight = (
  width: number,
  height: number,
  options?: Options
) => {
  const paddingBottom = (width / height) * 100;

  if (options?.staticHeight) {
    return '100%';
  }

  if (paddingBottom < 90) {
    return `${options?.minHeightValue || 90}${options?.dimension || '%'}`;
  }

  if (paddingBottom > 130) {
    return `${options?.maxHeightValue || 130}${options?.dimension || '%'}`;
  }

  return `${paddingBottom}${options?.dimension || '%'}`;
};
