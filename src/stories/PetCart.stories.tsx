import React from 'react';
import '../styles/main.scss';
import { Story, Meta } from '@storybook/react/types-6-0';
import image from '../static/images/development/pet.jpg';

import Pet from '../components/cards/Pet';
import { PetProps } from '../components/cards/Pet';

export default {
  title: 'Cards/Pet',
  component: Pet,
} as Meta;

const Template: Story<PetProps> = (args) => (
  <div style={{ maxWidth: 366 }}>
    <Pet {...args} />
  </div>
);

export const PetCard = Template.bind({});

PetCard.args = {
  image: image,
  userNickname: 'Nikki S.',
  petName: 'Luna',
  countVotes: 1231,
};
