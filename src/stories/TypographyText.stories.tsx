import React from 'react';
import '../styles/main.scss';
// also exported from '@storybook/react' if you can deal with breaking changes in 6.1
import { Story, Meta } from '@storybook/react/types-6-0';
import { Typography } from '@material-ui/core';

export default {
  title: 'Typography/Text',
} as Meta;

const Template: Story<{ children: React.ReactNode }> = (args) => (
  <>
    <div>
      <Typography variant="body1">{args.children}</Typography>
    </div>
    <br />
    <div>
      <Typography variant="body2">{args.children}</Typography>
    </div>
    <br />
    <div>
      <Typography variant="caption">{args.children}</Typography>
    </div>
    <br />
    <div>
      <Typography variant="overline">{args.children}</Typography>
    </div>
  </>
);

export const Text = Template.bind({});

Text.args = {
  children: 'Text',
};
