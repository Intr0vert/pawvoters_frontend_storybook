import React from 'react';
import '../styles/main.scss';
// also exported from '@storybook/react' if you can deal with breaking changes in 6.1
import { Story, Meta } from '@storybook/react/types-6-0';
import RadioGroup from '@material-ui/core/RadioGroup';

import { Radio, RadioProps } from '../components/ui/Radio';

export default {
  title: 'Form/Radio Button',
  component: Radio,
} as Meta;

const Template: Story<RadioProps> = (args) => <Radio {...args} />;

export const BaseRadio = Template.bind({});

BaseRadio.args = {
  disabled: false,
  label: 'Item',
};

export const DisabledRadio = Template.bind({});

DisabledRadio.args = {
  disabled: true,
  label: 'Item',
};

export const RadioGroupExample = () => {
  const [value, setValue] = React.useState('disabled');

  const handleChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    setValue((event.target as HTMLInputElement).value);
  };
  return (
    <RadioGroup
      aria-label="gender"
      name="gender1"
      value={value}
      onChange={handleChange}
    >
      <Radio value="female" label="Female" />
      <Radio value="male" label="Male" />
      <Radio value="other" label="Other" />
      <Radio value="disabled" disabled label="(Disabled option)" />
    </RadioGroup>
  );
};
