import React from 'react';
import '../styles/main.scss';
// also exported from '@storybook/react' if you can deal with breaking changes in 6.1
import { Story, Meta } from '@storybook/react/types-6-0';
import { Link } from '@material-ui/core';

export default {
  title: 'Typography/Link',
} as Meta;

const Template: Story<{ children: React.ReactNode }> = (args) => (
  <>
    <div>
      <Link href="http://www.google.com">{args.children}</Link>
    </div>
    <br />
    <div>
      <Link href="http://www.google.com" className="withIcon">
        <svg
          width="24"
          height="20"
          viewBox="0 0 24 20"
          fill="currentColor"
          xmlns="http://www.w3.org/2000/svg"
        >
          <path d="M12 10.586L16.243 14.828L14.828 16.243L13 14.415V20H11V14.413L9.17199 16.243L7.75699 14.828L12 10.586ZM12 2.2216e-08C13.717 8.17698e-05 15.3741 0.631114 16.6562 1.77312C17.9383 2.91512 18.7561 4.48846 18.954 6.194C20.1983 6.53332 21.2837 7.2991 22.0206 8.35753C22.7576 9.41595 23.0991 10.6997 22.9856 11.9844C22.8721 13.2691 22.3107 14.473 21.3995 15.3858C20.4884 16.2986 19.2855 16.8622 18.001 16.978L18 15C18.0016 13.4271 17.3855 11.9166 16.2843 10.7935C15.1832 9.67034 13.6851 9.02452 12.1125 8.99502C10.5399 8.96553 9.01865 9.55472 7.87617 10.6357C6.73369 11.7168 6.06139 13.2032 6.00399 14.775L5.99999 15V16.978C4.71544 16.8623 3.51239 16.2989 2.60112 15.3862C1.68985 14.4735 1.12831 13.2696 1.01466 11.9848C0.901007 10.7001 1.24247 9.41628 1.97935 8.35777C2.71624 7.29926 3.80169 6.53339 5.04599 6.194C5.24369 4.48838 6.06139 2.91491 7.34357 1.77287C8.62574 0.63082 10.2829 -0.000136703 12 2.2216e-08Z" />
        </svg>
        {args.children}
      </Link>
    </div>
    <br />
  </>
);

export const Links = Template.bind({});

Links.args = {
  children: 'Text',
};
