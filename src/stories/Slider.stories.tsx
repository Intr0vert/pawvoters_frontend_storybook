import React from 'react';
import '../styles/main.scss';
// also exported from '@storybook/react' if you can deal with breaking changes in 6.1
import { Story, Meta } from '@storybook/react/types-6-0';

import { Slider, SliderProps } from '../components/ui/Slider';

export default {
  title: 'Form/Slider',
  component: Slider,
} as Meta;

const Template: Story<SliderProps> = (args) => (
  <div style={{ maxWidth: 250, paddingTop: 50 }}>
    <Slider {...args} />
  </div>
);

export const SingleValue = Template.bind({});

SingleValue.args = {
  defaultValue: 10,
  step: 1,
  min: 0,
  max: 100,
};

export const MultipleValues = Template.bind({});

MultipleValues.args = {
  defaultValue: [-50, 25],
  step: 5,
  min: -150,
  max: 150,
};
