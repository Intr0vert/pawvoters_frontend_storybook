import React from 'react';
import '../styles/main.scss';
import { Meta } from '@storybook/react/types-6-0';
import 'react-toastify/dist/ReactToastify.css';
import { ToastContainer } from 'react-toastify';
import Button from '../components/ui/Button';
import { Close } from '../components/icons';
import { createErrorToast, createSuccessToast } from '../lib/utils/toasts';

export default {
  title: 'Components/Toasts',
} as Meta;

const Template = () => (
  <div>
    <ToastContainer
      limit={3}
      position={'bottom-left'}
      autoClose={5000}
      closeButton={
        <i>
          <Close />
        </i>
      }
    />
    <Button
      onClick={() => {
        createErrorToast({ message: 'Error' });
      }}
    >
      Error
    </Button>
    <br />
    <br />
    <Button
      onClick={() => {
        createSuccessToast({ message: 'Success' });
      }}
    >
      Success
    </Button>
  </div>
);

export const BreadcrumbsBase = Template.bind({});
