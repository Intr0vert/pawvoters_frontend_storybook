import React from 'react';
import '../styles/main.scss';
// also exported from '@storybook/react' if you can deal with breaking changes in 6.1
import { Story, Meta } from '@storybook/react/types-6-0';

import AddPet from '../components/cards/AddPet';

export default {
  title: 'Cards/Add Pet',
  component: AddPet,
} as Meta;

const Template: Story = () => (
  <div style={{ maxWidth: 284 }}>
    <AddPet />
  </div>
);

export const AddPetCard = Template.bind({});
