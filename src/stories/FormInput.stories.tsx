import React from 'react';
import '../styles/main.scss';
// also exported from '@storybook/react' if you can deal with breaking changes in 6.1
import { Story, Meta } from '@storybook/react/types-6-0';

import { Input, InputProps } from '../components/ui/Input';

export default {
  title: 'Form/Input',
  component: Input,
  args: {
    name: 'base_input',
    id: 'base_input',
    multiline: false,
  },
} as Meta;

const Template: Story<InputProps> = (args) => (
  <div style={{ width: 211 }}>
    <Input {...args} />
  </div>
);

export const BaseInput = Template.bind({});

BaseInput.args = {
  disabled: false,
  label: 'Title',
};

export const ErrorInput = Template.bind({});

ErrorInput.args = {
  error: true,
  label: 'Title',
  helperText: 'Error',
};

export const SuccessInput = Template.bind({});

SuccessInput.args = {
  success: true,
  label: 'Title',
  endIcon: (
    <svg
      width="16"
      height="16"
      viewBox="0 0 16 16"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
    >
      <mask
        id="mask0"
        mask-type="alpha"
        maskUnits="userSpaceOnUse"
        x="0"
        y="0"
        width="16"
        height="16"
      >
        <path
          d="M6.66666 10.1147L12.7947 3.986L13.738 4.92867L6.66666 12L2.424 7.75733L3.36666 6.81467L6.66666 10.1147Z"
          fill="#31344E"
        />
      </mask>
      <g mask="url(#mask0)">
        <rect width="16" height="16" fill="#9ECA74" />
        <rect width="16" height="16" fill="#9ECA74" />
      </g>
    </svg>
  ),
};

export const InputWithIcon = Template.bind({});

InputWithIcon.args = {
  label: 'Title',
  beginIcon: (
    <svg
      width="16"
      height="16"
      viewBox="0 0 16 16"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
    >
      <mask
        id="mask0"
        mask-type="alpha"
        maskUnits="userSpaceOnUse"
        x="0"
        y="0"
        width="16"
        height="16"
      >
        <path
          d="M12.0207 11.078L14.876 13.9327L13.9327 14.876L11.078 12.0207C10.0158 12.8722 8.69468 13.3353 7.33334 13.3333C4.02134 13.3333 1.33334 10.6453 1.33334 7.33334C1.33334 4.02134 4.02134 1.33334 7.33334 1.33334C10.6453 1.33334 13.3333 4.02134 13.3333 7.33334C13.3353 8.69467 12.8722 10.0158 12.0207 11.078ZM10.6833 10.5833C11.5294 9.71326 12.0019 8.54696 12 7.33334C12 4.75467 9.91134 2.66667 7.33334 2.66667C4.75468 2.66667 2.66668 4.75467 2.66668 7.33334C2.66668 9.91134 4.75468 12 7.33334 12C8.54696 12.0019 9.71327 11.5294 10.5833 10.6833L10.6833 10.5833Z"
          fill="#31344E"
        />
      </mask>
      <g mask="url(#mask0)">
        <rect width="16" height="16" fill="#A4AFBE" />
      </g>
    </svg>
  ),
};

export const DisabledInput = Template.bind({});

DisabledInput.args = {
  disabled: true,
  label: 'Title',
};

export const MultilineInput = Template.bind({});

MultilineInput.args = {
  label: 'Title',
  multiline: true,
  rows: 6,
};
