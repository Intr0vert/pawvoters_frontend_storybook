import React from 'react';
import '../styles/main.scss';
// also exported from '@storybook/react' if you can deal with breaking changes in 6.1
import { Story, Meta } from '@storybook/react/types-6-0';

import {
  RoundedButton,
  RoundedButtonProps,
} from '../components/ui/RoundedButton';

export default {
  title: 'Buttons/Rounded Button',
  component: RoundedButton,
} as Meta;

const Template: Story<RoundedButtonProps> = (args) => (
  <RoundedButton {...args} />
);

export const Primary = Template.bind({});

Primary.args = {
  disabled: false,
  children: (
    <svg
      width="16"
      height="16"
      viewBox="0 0 16 16"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
    >
      <mask
        id="mask0"
        mask-type="alpha"
        maskUnits="userSpaceOnUse"
        x="0"
        y="0"
        width="16"
        height="16"
      >
        <path
          d="M8.00001 14.6667C4.31801 14.6667 1.33334 11.682 1.33334 8.00004C1.33334 4.31804 4.31801 1.33337 8.00001 1.33337C11.682 1.33337 14.6667 4.31804 14.6667 8.00004C14.6667 11.682 11.682 14.6667 8.00001 14.6667ZM7.33334 7.33337H4.66668V8.66671H7.33334V11.3334H8.66668V8.66671H11.3333V7.33337H8.66668V4.66671H7.33334V7.33337Z"
          fill="black"
        />
      </mask>
      <g mask="url(#mask0)">
        <rect width="16" height="16" fill="#EB864B" />
      </g>
    </svg>
  ),
};
