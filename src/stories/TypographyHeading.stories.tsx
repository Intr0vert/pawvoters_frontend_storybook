import React from 'react';
import '../styles/main.scss';
// also exported from '@storybook/react' if you can deal with breaking changes in 6.1
import { Story, Meta } from '@storybook/react/types-6-0';
import { Typography } from '@material-ui/core';

export default {
  title: 'Typography/Heading',
} as Meta;

const Template: Story<{ children: React.ReactNode }> = (args) => (
  <>
    <div>
      <Typography variant="h1">{args.children}</Typography>
    </div>
    <br />
    <div>
      <Typography variant="h2">{args.children}</Typography>
    </div>
    <br />
    <div>
      <Typography variant="h3">{args.children}</Typography>
    </div>
    <br />
    <div>
      <Typography variant="h4">{args.children}</Typography>
    </div>
  </>
);

export const Heading = Template.bind({});

Heading.args = {
  children: 'Heading',
};
